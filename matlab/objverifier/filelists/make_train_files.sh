#!/bin/bash

DBPATH=/home/kamarain/Data/gabor_object_detector

# Make list of positive training examples
find ${DBPATH}/acoustic_guitar_FW_verification_pos -name \*.jpg > train_pos.txt
find ${DBPATH}/garden_spider_FW_verification_pos -name \*.jpg >> train_pos.txt
find ${DBPATH}/grey_owl_FW_verification_pos -name \*.jpg >> train_pos.txt
find ${DBPATH}/piano_FW_verification_pos -name \*.jpg >> train_pos.txt
find ${DBPATH}/snail_FW_verification_pos -name \*.jpg >> train_pos.txt

# Make list of negative training examples
find ${DBPATH}/acoustic_guitar_FW_verification_neg -name \*.jpg > train_neg.txt
find ${DBPATH}/garden_spider_FW_verification_neg -name \*.jpg >> train_neg.txt
find ${DBPATH}/grey_owl_FW_verification_neg -name \*.jpg >> train_neg.txt
find ${DBPATH}/piano_FW_verification_neg -name \*.jpg >> train_neg.txt
find ${DBPATH}/snail_FW_verification_neg -name \*.jpg >> train_neg.txt

# Make list of test examples
find ${DBPATH}/acoustic_guitar_FW_verification_neg -name \*.jpg > train_test.txt
find ${DBPATH}/garden_spider_FW_verification_neg -name \*.jpg >> train_test.txt
find ${DBPATH}/grey_owl_FW_verification_neg -name \*.jpg >> train_test.txt
find ${DBPATH}/piano_FW_verification_neg -name \*.jpg >> train_test.txt
find ${DBPATH}/snail_FW_verification_neg -name \*.jpg >> train_test.txt
