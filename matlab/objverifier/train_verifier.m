% Config part
conf.posFile = 'filelists/train_pos.txt';
conf.negFile = 'filelists/train_neg.txt';
conf.dbPath  = '/home/kamarain/Data/gabor_object_detector';
conf.subImgInds = 10:50; % for subset of colours to omit black
                         % border areas
conf.imgSize = [12 12]; % increase until PCA fails
conf.dataPCA = 10; % optimal should be between 5-20
conf.numOfNeurons = 20; % as few as possible to prevent overfitting
conf.useColourPCA = false;

%
% 1. Read training images
classLabels = {};
totPos = mvpr_lcountentries(conf.posFile);
fh = mvpr_lopen(conf.posFile,'read');
for posInd = 1:totPos
  fprintf('\r Reading positive examples %4d/%4d',posInd,totPos);
  fileEntry = mvpr_lread(fh);
  trainDataPos(posInd).className = fileEntry{1};
  trainDataPos(posInd).imgFile = fileEntry{2};
  classLabel = strmatch(fileEntry{1},classLabels);
  if isempty(classLabel)
    classLabels{length(classLabels)+1} = fileEntry{1};
    classLabel = length(classLabels)+1;
  end;
  trainDataPos(posInd).classLabel = classLabel;
end;
fprintf(' ...Done!\n');
mvpr_lclose(fh);

totNeg = mvpr_lcountentries(conf.negFile);
fh = mvpr_lopen(conf.negFile,'read');
for negInd = 1:totNeg
  fprintf('\r Reading negative examples %4d/%4d',negInd,totNeg);
  fileEntry = mvpr_lread(fh);
  trainDataNeg(negInd).className = fileEntry{1};
  trainDataNeg(negInd).imgFile = fileEntry{2};
  classLabel = strmatch(fileEntry{1},classLabels);
  if isempty(classLabel)
    warning([fileEntry{1} ' unknown label!']);
    %classLabels{length(classLabels)+1} = fileEntry{1};
    %classLabel = length(classLabels)+1;
    classLabel = -1;
  end;
  trainDataNeg(negInd).classLabel = classLabel;
end;
fprintf(' ...Done!\n');
mvpr_lclose(fh);

%
% 2. Transform from RGB to single value
if conf.useColourPCA
  % Find principal colours for each class
  fprintf('\r Computing colour PCA');
  for clInd = 1:length(classLabels)
    clInds = find([trainDataPos(:).classLabel] == clInd);
    P = [];
    for currInd = clInds
      img = imread(fullfile(conf.dbPath,trainDataPos(currInd).imgFile));
      if (length(size(img)) < 3)
        warning([trainDataPos(currInd).imgFile 'is gray scale, ' ...
                 'skipping']);
        continue;
      end;
      subImg = img(conf.subImgInds,conf.subImgInds,:);
      P = [P; reshape(subImg,[size(subImg,1)*size(subImg,2) 3])];
    end;
    P = double(P);
    [Pnew colorPCA(clInd).V colorPCA(clInd).M] = pcatransf(P,1);
  end;
else
  fprintf(' No colour PCA - using RGB2Gray ');
  fprintf(' ...Done!\n');
end;

%
% 3. For data vectors
% Create training data vectors
fprintf('Forming the training matrices...');
for clInd = 1:length(classLabels)
  % positive
  clIndsPos = find([trainDataPos(:).classLabel] == clInd);
  data(clInd).Ppos = [];
  for currInd = clIndsPos
    img = imread(fullfile(conf.dbPath,trainDataPos(currInd).imgFile));
    if (length(size(img)) < 3)
      warning([trainDataPos(currInd).imgFile 'is gray scale, ' ...
                          'skipping']);
      continue;
    end;
    sImg = imresize(img,conf.imgSize);
    if (conf.useColourPCA)
      Pimg = reshape(sImg,[size(sImg,1)*size(sImg,2) 3]);
      Pimg = double(Pimg);
      Pimg = pcatransf(Pimg,1,colorPCA(clInd).V,colorPCA(clInd).M);
    else
      sImg = rgb2gray(sImg);
      Pimg = reshape(sImg,[size(sImg,1)*size(sImg,2) 1]);
      Pimg = double(Pimg);
    end;
    data(clInd).Ppos = [data(clInd).Ppos; transpose(Pimg)];
  end;

  % negative
  clIndsNeg = find([trainDataNeg(:).classLabel] == clInd);
  data(clInd).Pneg = [];
  for currInd = clIndsNeg
    img = imread(fullfile(conf.dbPath,trainDataNeg(currInd).imgFile));
    if (length(size(img)) < 3)
      warning([trainDataNeg(currInd).imgFile 'is gray scale, ' ...
                          'skipping']);
      continue;
    end;
    sImg = imresize(img,conf.imgSize);
    if (conf.useColourPCA)
      Pimg = reshape(sImg,[size(sImg,1)*size(sImg,2) 3]);
      Pimg = double(Pimg);
      Pimg = pcatransf(Pimg,1,colorPCA(clInd).V,colorPCA(clInd).M);
    else
      sImg = rgb2gray(sImg);
      Pimg = reshape(sImg,[size(sImg,1)*size(sImg,2) 1]);
      Pimg = double(Pimg);
    end;
    data(clInd).Pneg = [data(clInd).Pneg; transpose(Pimg)];
  end;
end;
fprintf(' ...Done!\n');

%
% 4. Reduce the data dimensionality 
% PCA project of the data
fprintf('Data dim-reduction by PCA...');
for clInd = 1:length(classLabels)
  % PCA based on positive examples
  [dataPCA(clInd).Ppos dataPCA(clInd).V dataPCA(clInd).M] = ...
      pcatransf(data(clInd).Ppos, conf.dataPCA);
  % Transform of the negative as well
  dataPCA(clInd).Pneg = ...
      pcatransf(data(clInd).Pneg, conf.dataPCA,...
                dataPCA(clInd).V, dataPCA(clInd).M);
end;
fprintf(' ...Done!\n');

%
% 5. Train neural network
% train a neural network
fprintf('Training a classifier...');
for clInd = 1:length(classLabels)
  P = [dataPCA(clInd).Ppos; dataPCA(clInd).Pneg];
  T = [ones(size(dataPCA(clInd).Ppos,1),1); ...
       zeros(size(dataPCA(clInd).Pneg,1),1)];
  net{clInd} = patternnet([conf.numOfNeurons]);
  net{clInd} = train(net{clInd},P',T');
end;
fprintf(' ...Done!\n');

%
% 6. Test classifier
% Testing network with the training data
fprintf('Training set error...');
for clInd = 1:length(classLabels)
  P = [dataPCA(clInd).Ppos; dataPCA(clInd).Pneg];
  T = [ones(size(dataPCA(clInd).Ppos,1),1); ...
       zeros(size(dataPCA(clInd).Pneg,1),1)];
  T_h =  net{clInd}(P');
  % Compute precision-recall
  th_ind = 0;
  ths = [];
  for th = 0:0.1:1
    ths = [ths th];
    th_ind = th_ind+1;
    o = T_h > th;
    posInd = find(T == 1);
    re(clInd,th_ind) = sum((T(posInd)-o(posInd)') == 0)/length(posInd);
    pr(clInd,th_ind) = sum((T-o') == 0)/length(T);
  end;
  [minval minind] = min(abs(re(clInd,:)-pr(clInd,:)));
  eer(clInd) = min([re(clInd,minind) pr(clInd,minind)]);
  eer_th(clInd) = ths(minind);
end;
fprintf(' ...Done!\n');
fprintf('Training set EERs: ');
fprintf('%.2f ',eer);
fprintf('\n');
fprintf('EER tresholds: ');
fprintf('%.2f ',eer_th);
fprintf('\n');
plot(re',pr')
xlabel('recall');
ylabel('precision');
title('Training data precision-recall')
legend(classLabels)




