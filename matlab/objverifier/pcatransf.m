%PCATRANSF Principal component analysis transformation.
%   [P V M] = PCATRANSF(D, K) where D is a mxn data matrix of m
%   samples of n dimensional data. Function returns MxK matrix P,
%   where data is transformed into K first principal
%   dimensions. Also the principal components are returned in KxN
%   vector V and the mean value vector M. You can use V and M
%   vectors to transfer other data (test set) into same principal
%   component space.
%
%   >>[DataTrainPCA V M] = pcatransf(DataTrain,5);
%   >>[DataTestPCA] = pcatransf(DataTest,5,V,M);
%
%   See also COV, SVD.
function [pcaP,pcaV,pcaM] = pcatransf(data,k,varargin)

pcaVariablesGiven = 0;
%
% Parameter parsing
switch nargin
 case 2,

 case 4,
  pcaVariablesGiven = 1;
  pcaV = varargin{1};
  pcaM = varargin{2};
  if (size(pcaV,1) ~= size(data,2))
    error(['You gave pca vectors that do not have as many rows as' ...
	   ' columns in the data!!']);
  end;
  if (size(pcaM,2) ~= size(data,2))
    error(['You gave mean vector that do not have as many columns as' ...
	   ' columns in the data!!']);
  end;

 otherwise,
  error('Wrong number of parameters!');
end;  

% bad condition check
if nargin~=4 && (size(data,1) < size(data,2))
   error('Matrix is not sufficient data matrix (more dimensions than samples)');
end;

if (pcaVariablesGiven)
  pcaP = (data-repmat(pcaM,size(data,1),1))*pcaV;
else
  % center data before PCA
  [cdata pcaM] = center(data);
  
  [U S V] = svd(cdata,0);
  pcaProjection = U*S;
  pcaP = pcaProjection(:,1:k);
  pcaV = V(:,1:k); 
end;
  
% % this is too old and dummy way to do it
%	C = cov(data);
%	[V,D] = eig(C);
%	[tmp inds] = sort(diag(D)); 
%
%	dimData = size(data,2); % dimension
%	% last eigen value represents the first PC, so k last
%	pcaV=V(:,inds(dimData:-1:dimData-k+1));
%	%%%%%%%% ERROR IN NEXT LINE %%%%%%%%
%	pcaS = data*pcaV; % here is an error because data must be centered first
%end;
