%CENTER Center data matrix
%   [CDATA DMEAN] = CENTER(DATA) centers data matrix where DATA is a MxN data matrix
%   of M samples of N dimensional data. Function returns CDATA matrix where data
%   matrix is centered. 
%
%   See also MEAN.
function [cdata, datamean] = center(data)

[m,n] = size(data);
i     = ones(m,1);

datamean = mean(data);
cdata     = data - datamean(i,:);

