%O3D2D_CONST_PPMODEL_FILES - Creates 2D constellation model using
%                            landmark "point patterns"
%
% [ppModelS] = o3d2d_const_ppmodel_files(bboxFlag_, genLmFlag_, cls_,trainListFile_,varargin);
%
% This function loads ground truth image feature points and creates
% a mean point pattern model describing the average spatial
% constellation as described in [1,3].
%
% Output:
%
% Input:
%  trainListFile_ - File with full path containing training images
%                   and landmark files (if empty, then
%                   'reportFile' read from the 'tempSaveDir')
%  bboxFlag_      - defines the use of bounding box corners in the mean
%                   model (true/false)
%  genLmsFlag_    - used to chnge paths, when automatically generated
%                   landmarks are used
%  cls_           - name of the category of images
%
%  <optional>
% 'homography'    - {'none','isometry','similarity','affinity','projectivity'}
%                   (Def. 'similarity'), see [1,2]
% 'lmpdf_gmmbEMComponents' - Def. 1 and anything else provides _troubles_
% 'lmpdf_gmmbHistApproxPoints' - Number of points used for
%                                generating the probability score
%                                (Def. 1000) - see ref. [4]
% 'reportFile'    - Image list file name used if trainListFile_ empty
%                   (Def. 'validatetraindata_accepted.txt')
% 'landmarkRoot'  - '.'
% 'tempSaveDir'   - Output directory for all produced files
%                   (Def: '/tmp') - Note that you should specify
%                   this carefully as the system temp is cleaned
%                   periodically
% 'constModelSaveFile' - Save file name (Def. 'const_ppmodel_save.mat')
% 'debugLevel'    - 0,1,.. (Def. 0)
%
% See also O3D2D_EVEX_VALIDATETRAINDATA_FILES.M and
% O3D2D_EVEX_MEANMODEL_FILES.M .
%
% References:
%
%  [1] Kamarainen, J.-K., Supervised Learning of Probabilistic
%  Part-Based Models for Object Detection and Localisation, IEEE
%  PAMI 2012.
%
%  [2] Kamarainen, J.-K., Ilonen, J., Learning and Detection of
%  Object Landmarks in Canonical Object Space, 20th Int. Conf. on
%  Pattern Recognition (ICPR2010)  (Istanbul, Turkey, 2010).
%
%  [3] Object Localisation Using Generative Probability Model for
%  Spatial Constellation and Local Image Features
%  (J.-K. Kamarainen, M. Hamouz, J. Kittler, P. Paalanen,
%  J. Ilonen, A. Drobchenko), In ICCV Workshop on Non-Rigid
%  Registration and Tracking Through Learning, 2007.
%
%  [4] Gaussian mixture pdf in one-class classification: computing
%  and utilizing confidence values (J. Ilonen, P. Paalanen,
%  J.-K. Kamarainen, H. Kälviäinen), In 18th Int. Conf. on Pattern
%  Recognition (ICPR2006), 2006.
%
% Author(s):
%    Joni Kamarainen <Joni.Kamarainen@lut.fi>
%
% Copyright:
%
%   Object3Dto2D project 2D constellation model (*_const_*) toolbox is
%   Copyright (C) 2006-2012 by Joni-Kristian Kamarainen.
function [ppModelS] = o3d2d_const_ppmodel_files(bboxFlag_, genLmFlag_, cls_,...
    trainListFile_,varargin)

% 1. Parse input arguments
conf = struct(...
    'homography', 'similarity',...
    'lmpdf_gmmbEMComponents', 1,...
    'lmpdf_gmmbHistApproxPoints', 1000,...
    'reportFile', 'validatetraindata_accepted.txt',...
    'debugLevel', 0,...
    'tempSaveDir', '/tmp',...
    'constModelSaveFile', 'const_ppmodel_save.mat',...
    'landmarkRoot', '.');
conf = mvpr_getargs(conf,varargin);

ppModelS.homography = 'similarity';%conf.homography;

%
% 2. Form the mean model space (using landmarks) where the
% landmarks are mapped and the spatial priors formed
fprintf('   Forming the mean point pattern model...\n');

if (isempty(trainListFile_))
    [ppModelS.meanModel ppModelS.meanModelMGD transExamples transfLMs] = ...
        o3d2d_evex_meanmodel_files2(fullfile(conf.tempSaveDir,conf.reportFile),...
        bboxFlag_,...
        genLmFlag_,...
        cls_,...
        conf.homography,...
        'tempSaveDir', conf.tempSaveDir,...
        'landmarkRoot', conf.landmarkRoot,...
        'debugLevel', conf.debugLevel,...
        'forceSeed',true);
    
else
    [ppModelS.meanModel ppModelS.meanModelMGD transExamples transfLMs] = ...
        o3d2d_evex_meanmodel_files2(trainListFile_,...
        bboxFlag_,...
        genLmFlag_,...
        cls_,...
        conf.homography,...
        'tempSaveDir', conf.tempSaveDir,...
        'landmarkRoot', conf.landmarkRoot,...
        'debugLevel', conf.debugLevel,...
        'forceSeed',true);
    
end;

fprintf('   Done (err = %4.2f pixels)!!\n',ppModelS.meanModelMGD);
%
% 3. Create the local spatial distributions for every landmark
fprintf(['   Constructing pdfs (single Gaussian) for every landmark ' ...
    'in the mean model space...']);

transExamples = transfLMs;

TtransExamples = permute(transExamples,[3 2 1]);
TtransExamples = reshape(TtransExamples,...
    [2 size(TtransExamples,2)*size(TtransExamples,3)])';
Tclasses = repmat([1:size(transExamples,2)]',...
    [size(transExamples,1) 1]);

% add some noise for making probabilities finite (for canonical ims)
fh = mvpr_lopen(fullfile(conf.tempSaveDir,conf.reportFile), 'read');
filepair = mvpr_lread(fh);
if ~isempty(strfind(filepair{1},'canonical'))
    TtransExamples = TtransExamples + randn(size(TtransExamples))*3;
end
mvpr_lclose(fh);
%%
ppModelS.localDistrGMMB = gmmb_create(TtransExamples, Tclasses, 'EM','components',...
    conf.lmpdf_gmmbEMComponents');

ppModelS.localDistrGMMBHistS = gmmb_generatehist(ppModelS.localDistrGMMB, ...
    conf.lmpdf_gmmbHistApproxPoints);

fprintf('Done!\n');

%
% 3.1 Replace mean model with the new estimates
for (featNo = 1:size(ppModelS.localDistrGMMB,2))
    ppModelS.meanModel(featNo,:) = ppModelS.localDistrGMMB(featNo).mu';
end;

%
% 3.2 Compute limits for std1, std2, std3 and std4
for evInd = 1:size(ppModelS.meanModel,1)
    covEv = ppModelS.localDistrGMMB(evInd).sigma;
    if (sum(covEv(:).^2) > eps ) % non-singular
        ppModelS.std1pdf(evInd) = gmmb_cmvnpdf((chol(covEv)' * [1; 1])',...
            [0 0], covEv);
        ppModelS.std2pdf(evInd) = gmmb_cmvnpdf((chol(covEv)' * [2; 2])',...
            [0 0], covEv);
        ppModelS.std3pdf(evInd) = gmmb_cmvnpdf((chol(covEv)' * [3; 3])',...
            [0 0], covEv);
        ppModelS.std4pdf(evInd) = gmmb_cmvnpdf((chol(covEv)' * [4; 4])',...
            [0 0], covEv);
    else
        error('Singular covariance!');
    end;
end;

% mmLMarks = transfLMs;
mmLMarks = transfLMs(1:50,1:end,:);
% mmLMarks = lMarks(1:50,1:end-4,:);
%%%%%%%%%%%% DEBUG [1] %%%%%%%%%%%%%%%%%%%%
if (conf.debugLevel >= 1)
    
    plot(ppModelS.meanModel(:,1),ppModelS.meanModel(:,2),'bd','LineWidth',2,'MarkerSize',15);
    clf;cla;
    %axis([0.9*min(meanModel(:,1)) 1.1*max(meanModel(:,1))...
    %      0.9*min(meanModel(:,2)) 1.1*max(meanModel(:,2))]);
    set(get(gcf,'CurrentAxes'),'YDir','reverse');
    axis off
    hold on;
    plot(mmLMarks(:,:,1),... % These help to plot axes properly (yes, a kludge)
        mmLMarks(:,:,2),'w.');
    for dbgInd = 1:size(mmLMarks,1)
        for dbgInd2 = 1:size(mmLMarks,2)
            text(mmLMarks(dbgInd,dbgInd2,1),...
                mmLMarks(dbgInd,dbgInd2,2),...
                num2str(dbgInd2),...
                'FontSize',26,'Color','black',...
                'BackgroundColor','white','EdgeColor','black');
            %plot(mmLMarks(dbgInd,:,1),mmLMarks(dbgInd,:,2),'r.','LineWidth',2,'MarkerSize',15);
        end;
    end;
    for dbgInd2 = 1:size(mmLMarks,2)
        text(ppModelS.meanModel(dbgInd2,1),...
            ppModelS.meanModel(dbgInd2,2),...
            num2str(dbgInd2),...
            'FontSize',28,'Color','black',...
            'BackgroundColor','yellow','EdgeColor','black');
    end;
    
    hold on;
    for evInd = 1:size(ppModelS.meanModel,1)
        meanEv = ppModelS.localDistrGMMB(evInd).mu;
        covEv = ppModelS.localDistrGMMB(evInd).sigma;
        %     figd = plot(meanEv(1),meanEv(2),'rx');
        %     set(figd,'MarkerSize',14);
        %     set(figd,'LineWidth',3);
        if (sum(covEv(:).^2) > eps ) % non-singular
            [x,y,z] = cylinder([2 2],40); % two times std
            xy = [x(1,:); y(1,:)];
            mxy = chol(covEv)' * xy;
            x = mxy(1,:) + meanEv(1);
            y = mxy(2,:) + meanEv(2);
            figd = plot(x,y,'r-');
            set(figd,'LineWidth',4);
        end;
        %figd = text(5+meanEv(1),5-meanEv(2),num2str(evInd));
    end;
    hold off;
    input('Debug[1] - Trained local distributions (2 X std surfaces) <RETURN>');
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ppModelS.category = cls_;

% Save constellation model
save(fullfile(conf.tempSaveDir,conf.constModelSaveFile), 'ppModelS');
fprintf(['Saved the constellation model to ''' ...
    fullfile(conf.tempSaveDir,conf.constModelSaveFile) ...
    '''\n']);
