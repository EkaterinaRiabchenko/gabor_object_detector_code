function conf = initialization(category)
conf.category = category;

conf.skip_validation = false;
conf.skip_constmodel = false;
conf.skip_findhypotheses = false;
conf.skip_evaluation = false;
conf.box = true;
conf.bbLms = false;
conf.debugLevel = 2;
db = 'caltech';

conf.caltech101_dir = '~/svn/backup/partdetector/base/caltech101';

switch conf.category
 case 'foo'
  % train and test files
  conf.file_list_train = sprintf('filelists/%s_train.txt',conf.category);
  conf.file_list_test = sprintf('filelists/%s_tets.txt',conf.category);
  conf.temp_save_dir = sprintf('TEMPSAVE/caltech101_demo/%s',conf.category);
  conf.lm_temp_save_dir = sprintf('../partdetector/TEMPSAVE/caltech101_demo/',conf.category);
  conf.numOfHypotheses = 10;
  
 otherwise
  % train and test files
   conf.file_list_train = sprintf('filelists/%s_train2.txt',conf.category);
   conf.file_list_test = sprintf('filelists/%s_test2.txt',conf.category);
   conf.file_list_validate = sprintf('filelists/%s_validate2.txt',conf.category);
  conf.temp_save_dir = sprintf('TEMPSAVE/caltech101_demo/%s',db);
  conf.classifier_temp_dir = sprintf('../partdetector/TEMPSAVE/caltech101_demo/%s',conf.category);
  conf.lm_temp_save_dir = sprintf('../partdetector/TEMPSAVE/caltech101_demo/%s',conf.category);
  conf.numOfHypotheses = 10;
end
end
