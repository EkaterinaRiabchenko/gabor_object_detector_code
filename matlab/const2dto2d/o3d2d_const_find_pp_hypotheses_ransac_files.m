%O3D2D_CONST_FIND_PP_HYPOTHESES_RANSAC_FILES - Find hypotheses of
%                          object locations using the point pattern
%                          constellation model, file mode
%
% [] = o3d2d_const_find_pp_hypotheses_ransac_files(testImagesListFile_,mode_,ppModelS_,bboxFlag_,category_,:)
%
% This function runs O3D2D_CONST_FIND_PP_HYPOTHESES_RANSAC() for
% every entry in testImagesListFile_ . The file format is
% <IMGFILE> <DOTFILE>
% ....       ....
%
% Landmarks and other data are sought based on these names and the
% given 'lm_testSaveDir'.
%
% Output:
%
% Input:
%  testImagesListFile_ - File containing test images.
%  mode_               - {'test'/'train'}
%  ppModelS_           - Contains constellation model
%  bboxFlag_           - defines the use of bounding box corners in the mean
%                        model (true/false)
%  category_           - Name of the object category
%
%  <optional> All parameters of O3D2D_CONST_FIND_PP_HYPOTHESES_RANSAC() plus the following:
% 'hypNum'             - Number of the best hypotheses returned (Def. 10)
% 'lm_maxNum'          - Number of each landmark type used in hypothesis search (Def. 100)
% 'lm_sc_th'           - Landmark likelihood score threshold (useful
%                        values 0, 0.68, 0.95, 0.99, 0.999 corr. 0, std1,
%                        std2, etc (Def. 0) - usage questionable
% 'lm_tempSaveDir'     - Directory under which the extracted landmarks
%                        can be found (and their classifier structure) (Def. '')
% 'constModelSaveFile' - Save file name (Def. 'const_ppmodel_save.mat')
% 'lmpdf_gmmbHistApproxPoints' - Number of points used for
%                                generating the probability score
%                                (Def. 1000) - see ref. [4]
% 'tempSaveDir'        - Output directory for all produced files
%                        (Def: '/tmp') - Note that you should specify
%                        this carefully as the system temp is auto-cleaned
% 'debugLevel'     - 0,1,.. (Def. 0)
% 'debugImageRoot' - Dir under where images can be found (used by debug).
% 'landmarkRoot'  - '.'
%
% See also O3D2D_CONST_PPMODEL_FILES.M .
%
% References:
%
%  [1] Kamarainen, J.-K., Supervised Learning of Probabilistic
%  Part-Based Models for Object Detection and Localisation, IEEE
%  PAMI 2012.
%
%  [2] Kamarainen, J.-K., Ilonen, J., Learning and Detection of
%  Object Landmarks in Canonical Object Space, 20th Int. Conf. on
%  Pattern Recognition (ICPR2010)  (Istanbul, Turkey, 2010).
%
%  [3] Object Localisation Using Generative Probability Model for
%  Spatial Constellation and Local Image Features
%  (J.-K. Kamarainen, M. Hamouz, J. Kittler, P. Paalanen,
%  J. Ilonen, A. Drobchenko), In ICCV Workshop on Non-Rigid
%  Registration and Tracking Through Learning, 2007.
%
%  [4] Gaussian mixture pdf in one-class classification: computing
%  and utilizing confidence values (J. Ilonen, P. Paalanen,
%  J.-K. Kamarainen, H. Kälviäinen), In 18th Int. Conf. on Pattern
%  Recognition (ICPR2006), 2006.
%
%  [5] Riabchenko, E., Kamarainen, J.-K., Chen, Ke, Density-Avare Part-Based
%  Object Detection with Positive Examples, 22th Int. Conf. on
%  Pattern Recognition (ICPR2014)  (Stockholm, Sweden, 2014)
%
% Author(s):
%    Joni Kamarainen <Joni.Kamarainen@lut.fi>
%    Ekaterina Riabchenko <Ekaterina.Riabchenko@lut.fi>
%
% Copyright:
%
%   Object3Dto2D project 2D constellation model (*_const_*) toolbox is
%   Copyright (C) 2006-2012 by Joni-Kristian Kamarainen.
function [] = o3d2d_const_find_pp_hypotheses_ransac_files(testImagesListFile_,mode_,ppModelS_,bboxFlag_,category_,varargin)

% 1. Parse input arguments
conf = struct(...
    'hypNum', 10,...
    'lm_maxNum', 10,...
    'lm_sc_th', 0,...
    'clsr_tempSaveDir','',...
    'lm_tempSaveDir', '',...
    'constModelSaveFile', 'const_ppmodel_save.mat',...
    'lmpdf_gmmbHistApproxPoints', 1000,...
    'tempSaveDir', '/tmp',...
    'debugLevel', 0,...
    'debugImageRoot', '');
conf = mvpr_getargs(conf,varargin);

% 2. Read the point pattern model if not given
if (isempty(ppModelS_))
    fprintf(['   Loading point pattern constellation model  ''' ...
        fullfile(conf.tempSaveDir,conf.constModelSaveFile) ...
        '''...']);
    try
        load(fullfile(conf.tempSaveDir,conf.constModelSaveFile),...
            'ppModelS');
    catch
        error('Point pattern constellation model structure empty and cannot load one!');
    end;
    fprintf('done!\n');
else
    ppModelS = ppModelS_;
    clear ppModelS_;
end;

% Resolve some features
if bboxFlag_
    numOfFeatureClasses = size(ppModelS.meanModel,1) - 4;
else
    numOfFeatureClasses = size(ppModelS.meanModel,1);
end
numOfCoordDims = size(ppModelS.meanModel,2);

% Load the evidence classifier and form the likelihood score structure
evClassifierSaveFile = [fullfile(conf.lm_tempSaveDir,...
    'trainfeatureclassifier_classifier.mat')];
if (exist(evClassifierSaveFile,'file'))
    load(evClassifierSaveFile,'classifierS');
    
    try (classifierS{1})
        lm_localDistrGMMBHistS = gmmb_generatehist(classifierS{1}(1:numOfFeatureClasses),...
            conf.lmpdf_gmmbHistApproxPoints);
    catch
        lm_localDistrGMMBHistS = gmmb_generatehist(classifierS(1:numOfFeatureClasses),...
            conf.lmpdf_gmmbHistApproxPoints);
    end
else
    warning(['No classifier structure for the landmarks - score ' ...
        'computation disabled.']);
    classifierS = [];
end;

%
% 3. The main loop where images are loaded and processed one by one
fileCount = mvpr_lcountentries(testImagesListFile_);

fh = mvpr_lopen(testImagesListFile_, 'read');
fprintf('   Searching hypotheses from the test images...\n');
bestScores = [];
bestConstScores = [];
for imgInd = 1:fileCount
    fprintf('\r      File %5d/%d',imgInd,fileCount)
    
    % Load detected landmarks and give them for processing
    filepair = mvpr_lread(fh);
    [evDir foo evExt] = fileparts(filepair{1}); % 2->1
    [foo evFile foo] = fileparts(filepair{2});
    
    detSaveFile = [fullfile(conf.lm_tempSaveDir,evDir,evFile) '.det'];
    evids = load(detSaveFile);
    evidLabels = squeeze(evids(:,1));
    evidCoords = squeeze(evids(:,2:3));
    evidLhoods = squeeze(evids(:,4));
    if (conf.debugLevel > 0)
        if (exist(fullfile(conf.debugImageRoot,filepair{1}),'file'))
            debugImg = imread(fullfile(conf.debugImageRoot,filepair{1}));
        else
            debugImg = [];
            warning('In debug mode, but no debug image found!');
        end;
    else
        debugImg = [];
    end;
    
    % Get only the required number of landmarks
    subEvidCoords = [];
    subEvidLabels = [];
    subEvidLhoods = [];
    subEvidLhoodScores = [];
    for labelInd = 1:numOfFeatureClasses
        subInds = find(evidLabels == labelInd);
        subInds = subInds(1:min([length(subInds) conf.lm_maxNum]));
        subEvidCoords = [subEvidCoords; evidCoords(subInds,:)];
        subEvidLabels = [subEvidLabels; evidLabels(subInds)];
        subEvidLhoods = [subEvidLhoods; evidLhoods(subInds)];
        subEvidLhoodScores = [];
    end;
    
    % Further filter out poor score (low likelihood) landmarks
    subEvidScoreInds = 1:size(subEvidLabels,1);%find(subEvidLhoodScores >= conf.lm_sc_th);
    subEvidCoords = subEvidCoords(subEvidScoreInds,:);
    subEvidLabels = subEvidLabels(subEvidScoreInds);
    subEvidLhoods = subEvidLhoods(subEvidScoreInds);
    
    % add at least one landmark per category
    for labelInd = 1:numOfFeatureClasses
        if (sum(subEvidLabels==labelInd) == 0)
            subInds = find(evidLabels == labelInd);
            subEvidCoords = [subEvidCoords; evidCoords(subInds(1),:)];
            subEvidLabels = [subEvidLabels; evidLabels(subInds(1))];
            subEvidLhoods = [subEvidLhoods; evidLhoods(subInds(1))];
        end;
    end;
    
    % Execute the actual search for this image
    [hypS, bestScore] = o3d2d_const_find_pp_hypotheses_ransac2(subEvidLabels,subEvidCoords,...
        ppModelS, conf.hypNum, bboxFlag_,...
        'lm_lhoods', subEvidLhoods,...
        'lm_lhood_scores', [],...%subEvidLhoodScores,...
        'debugLevel', conf.debugLevel,...
        'tempSaveDir', conf.tempSaveDir,...
        'debugImg', debugImg);
    
    bestScores = [bestScores, bestScore(:)];
    % Store the best hypotheses
    hypSaveFile = fullfile(conf.tempSaveDir,[evDir '_' category_],...
        [evFile '.hyp']);
    %     make save dir if not exist
    if (~isdir(fullfile(conf.tempSaveDir,[evDir '_' category_])))
        mkdir(fullfile(conf.tempSaveDir,[evDir '_' category_]));
    end;
    
    % force all the corners of the bb to be inside of the image
    goodHyp = [];
    Image = imread(fullfile(conf.debugImageRoot,filepair{1}));
    
    [height,width,dim] = size(Image);
    try
        for hypInd = 1:size(hypS,1)
            bbHyp = squeeze(hypS(hypInd,end-3:end,:));
            bbHyp(bbHyp<1) = 1;
            bbHypX = bbHyp(:,1);
            bbHypY = bbHyp(:,2);
            bbHypY(bbHypY>height) = height;
            bbHypX(bbHypX>width) = width;
            bbHyp = [bbHypX, bbHypY];
            newHyp = shiftdim(bbHyp,-1);
            hypS(hypInd,end-3:end,:) = newHyp;
        end
    catch
        warning('no good hypothesis');
    end
    
    hypFh = mvpr_lopen(hypSaveFile,'write');
    for hypInd = 1:size(hypS,1)
        for evInd = 1:size(hypS,2)
            fprintf(hypFh.fid,'%f %f %d ',...
                hypS(hypInd,evInd,1),...  % x
                hypS(hypInd,evInd,2));  % y
        end;
        fprintf(hypFh.fid,'\n');
    end;
    mvpr_lclose(hypFh);
    
end;

scoreSaveFile = fullfile(conf.tempSaveDir,sprintf('bestScores_%s_%s.mat',category_,mode_));
save(scoreSaveFile,'bestScores');
mvpr_lclose(fh);
fprintf('   Done!\n');
