           Part-Based Object Detection and Localisation Toolbox V0.1.0
           -----------------------------------------------------------


References:

1. Changelog
------------

Version 0.1.0
  - Initial release


2. Known bugs
-------------

2.1 Critical (must be fixed)
- - - - - - - - - - - - - - - - - 
-

2.2 Less critical
- - - - - - - - -

1. If one or more landmarks is not found at all from the test image, then the method fails (kludge
   solution: add at least one landmark to anywhere in the image, even (Inf,Inf))

3. Short functional description and usage
-----------------------------------------

3.1 User functions (main interface)

[o3d2d_evex_evaluatetestresults_files.m]


3.2 Internal functions

-

4. TODO
-------

4.1 Very Important
- - - - - - - - - 
0. Study the different scoring approaches in ransac matching !!
   * The basic seems to work well with Faces_easy but poor with car_side
1. Take no fixed number of landmarks, but for example all those whose likelihood is within a fixed
   probability area (e.g. 95%)
2. Take no fixed number of hypotheses, but for example all those whose likelihood is within a fixed
   probability area (e.g. 95%)
3. In o3d2d_const_find_pp_hypotheses_ransac - for probScore, should we remove those local parts used in the estimation (they are always on the top probabilities and thus dense likelihood of two matches can outperform 2+2 landmarks of less dense distributions)?
4. How robust is the probability score conversion algorithm (effect of lmpdf_gmmbHistApproxPoints)

4.2 Less important
- - - - - - - - - 

