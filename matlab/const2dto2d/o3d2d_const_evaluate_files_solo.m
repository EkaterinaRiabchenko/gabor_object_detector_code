%O3DTO2D_CONST_EVALUATE_FILES_SOLO - evaluates suggested hypotheses of
%                                    objects' locations (and im classifications)
%
%[] = o3d2d_const_evaluate_files_solo(testListFile_,bboxFlag_,genLmFlag_,:)
%
% Output:
%
% Input:
%  trainListFile_ - File with full path containing training images
%                   and landmark files (if empty, then
%                   'reportFile' read from the 'tempSaveDir')
%  bboxFlag_      - defines the use of bounding box corners in the mean
%                   model (true/false)
%  genLmsFlag_    - used to chnge paths, when automatically generated
%                   landmarks are used
%
%  <optional>
% 'landmarkRoot'  - '.'
% 'imageRoot'  - '.'
% 'tempSaveDir'   - Output directory for all produced files
%                   (Def: '/tmp') - Note that you should specify
%                   this carefully as the system temp is cleaned
%                   periodically
% 'debugLevel'    - 0,1,.. (Def. 0)
%
% 
% Author(s):
%    Ekaterina Riabchenko <Ekaterina.Riabchenko@lut.fi>
% 

function [] = o3d2d_const_evaluate_files_solo(testListFile_,bboxFlag_,genLmFlag_,varargin);

% 1. Parse input arguments
conf = struct(...
    'imageRoot', '.',...
    'landmarkRoot', '.',...
    'tempSaveDir', '/tmp',...
    'debugLevel', 0);
conf = mvpr_getargs(conf,varargin);

n_categories = mvpr_lcountentries('listOfCategories.txt');

fid = fopen('listOfCategories.txt');
for i = 1:n_categories
    category = fgetl(fid);
    classes{i} = category;
    % might need to comment next line, if the test file is the same for all
    % categories
    testListFile_ = sprintf('~/svn/backup/partdetector/base/caltech101/filelists/%s_test2.txt',category);
    
    gtS{i} = load(fullfile(conf.tempSaveDir,sprintf('groundtruth_%s.mat',category)));
    bestScoresS{i} = load(fullfile(conf.tempSaveDir,sprintf('bestScores_%s_%s.mat',category,'test')));
    % used in visualisation, ordering detections according to their scores
    [val, ind] = sort(bestScoresS{i}.bestScores(1,:),'descend');
    tmp = 1:length(bestScoresS{i}.bestScores);
    rank(ind) = tmp;
    %here we use just 1 best score (have 10 corresponding to  the 10 best hyps)
    bestScoresS{i}.bestScores = bestScoresS{i}.bestScores(1,:);
    
    if ~isdir(fullfile('~/Desktop','detections',category))
        mkdir(fullfile('~/Desktop','detections',category));
    end
    
    fileCount = mvpr_lcountentries(testListFile_);
    fh = mvpr_lopen(testListFile_, 'read');
    fprintf('   Reading hypotheses for each test image...\n');
    tot_avg_ndists = [];
    if bboxFlag_
        for imgInd = 1:fileCount
            fprintf('\r      File %5d/%d',imgInd,fileCount)
            
            % Load detected landmarks and give them for processing
            filepair = mvpr_lread(fh);
            [evDir foo evExt] = fileparts(filepair{1});
            [foo evFile foo] = fileparts(filepair{2});
            
            hypSaveFile = fullfile(conf.tempSaveDir,[evDir '_' category],[evFile '.hyp']);
            hypotheses = load(hypSaveFile);
            hypotheses = reshape(hypotheses', [2 size(hypotheses,2)/2 size(hypotheses,1)]);
            hyp1 = hypotheses(:,:,1);
            if sum(hyp1(:)==0)
                bbres = zeros(4,2);
            else
                bbres = hyp1(:,(end-3):end)';
            end
            
            % Load ground truth bounding box and object parts locations
            [lmDir lmName foo] = fileparts(filepair{2});
            if genLmFlag_
                lmDir = ['Generated_' lmDir];
                lmgt = load(fullfile(conf.landmarkRoot, lmDir,category,[lmName '.dot']));
            else
                lmgt = load(fullfile(conf.landmarkRoot, lmDir,[lmName '.dot']));
            end
            bbgt = load(fullfile(conf.landmarkRoot, lmDir,[lmName '.box']));
            
            % calculate intersection score (intersection area/union area)
            % for true categories, otherwise it is zero
            if gtS{i}.gt(imgInd)==1
                
                [intersectx,intersecty]=curveintersect([bbres(:,1); bbres(1,1)],[bbres(:,2); bbres(1,2)],...
                    [bbgt(:,1); bbgt(1,1)],[bbgt(:,2); bbgt(1,2)]);
                
                ingt = inpolygon(bbres(:,1),bbres(:,2),bbgt(:,1),bbgt(:,2));
                inres = inpolygon(bbgt(:,1),bbgt(:,2),bbres(:,1),bbres(:,2));
                
                coordx = [intersectx; bbres(find(ingt==1),1); bbgt(find(inres==1),1)];
                coordy = [intersecty; bbres(find(ingt==1),2); bbgt(find(inres==1),2)];
                
                uncoordx = [intersectx; bbres(find(ingt==0),1); bbgt(find(inres==0),1)];
                uncoordy = [intersecty; bbres(find(ingt==0),2); bbgt(find(inres==0),2)];
                
                [theta, rho] = cart2pol((coordx-repmat(mean(coordx),length(coordx),1)),...
                    (coordy-repmat(mean(coordy),length(coordy),1)));
                [foo seqind] = sort(theta);
                
                if length(seqind)==0
                    intersectArea = 0;
                else
                    intersectArea = polyarea(coordx(seqind), coordy(seqind));
                end
                
                [untheta, unrho] = cart2pol((uncoordx-repmat(mean(uncoordx),length(uncoordx),1)),...
                    (uncoordy-repmat(mean(uncoordy),length(uncoordy),1)));
                [foo unseqind] = sort(untheta);
                unionArea = polyarea(uncoordx(unseqind), uncoordy(unseqind));
                score{i}(imgInd) = intersectArea/unionArea;
            else
                score{i}(imgInd) = 0;
            end
            if (conf.debugLevel==2)
                f = figure('Visible','off');
                imshow(fullfile(conf.imageRoot,filepair{1}));
                hold on;
                line([bbgt(:,1); bbgt(1,1)], [bbgt(:,2); bbgt(1,2)],'Color','y','LineWidth',4);
                line([bbres(:,1); bbres(1,1)], [bbres(:,2); bbres(1,2)],'Color','r','LineWidth',3);
                plot(hyp1(1,:), hyp1(2,:),'xr','MarkerSize',14,'LineWidth',4);
                plot(lmgt(:,1), lmgt(:,2),'oy','MarkerSize',14,'LineWidth',4);
                hold off;
                title(sprintf('%s, rank:%d, score:%d',category,rank(imgInd),bestScoresS{i}.bestScores(imgInd)));
                saveas(gcf,fullfile('~/Desktop','detections',category,[num2str(rank(imgInd)) '_' lmName '_bb.png']),'png');
                close(f)
            end
            numOfLms(imgInd,i) = size(hyp1,2);
        end;
        res1{i} = 1-score{i};
    end
end
% fclose('all');
numOfLms = max(numOfLms) - 4;

if 0%when the same file is used for testing all categories
    for i = 1:n_categories
        bestScores(i,:) = bestScoresS{i}.bestScores;
        gt(i,:) =  gtS{i}.gt;
    end
    
    bestScores(find(bestScores==0)) = realmin;
    bestScores = log10(bestScores);
    bestScores = bestScores./repmat(numOfLms',1,fileCount);
    
    
    [maxScores,tmp] = max(bestScores);
    
    res = repmat(tmp,size(gt,1),1).*gt;
    
    for i = 1:n_categories_
        for j = 1:n_categories_
            confMatr(i,j) = sum(res(i,:)==j)/sum(gt(i,:));
        end
    end
    confMatr
    prob = 10.^(bestScores)./repmat(sum(10.^bestScores),n_categories_,1);
end

%% detection precision-recall curve

% col = ['g', 'm', 'k', 'b', 'r', 'y','c','g','m','k','b'];
figure;
col = {'g-', 'm-', 'k-', 'b-', 'r-', 'y-','c-','g--','m--','k--','b--', 'r--', 'y--','c--'};
fid = fopen('listOfCategories.txt');
classes{n_categories+1} = 'ERR';
for i = 1:n_categories
    cls = fgetl(fid);
    [scr,ind] = sort(bestScoresS{i}.bestScores,'descend');
    foo = zeros(1,length(score{i}));
    foo(find(score{i}(ind)>0.5)) = 1;
    %         foo = ones(1,length(score));%makes without intersection threshold
    tp = foo.*gtS{i}.gt(ind);
    fn = (1-foo).*gtS{i}.gt(ind);
    fp = (1-foo).*(bestScoresS{i}.bestScores(ind)>0);
    fp=cumsum(fp);
    tp=cumsum(tp);
    fn=cumsum(fn);
    rec=tp/sum(gtS{i}.gt>0);% rec = tp./(tp+fn);
    prec=tp./(fp+tp);
    mrec=[0 ; rec' ; 1];
    mpre=[0 ; prec' ; 0];
    for j=numel(mpre)-1:-1:1
        mpre(j)=max(mpre(j),mpre(j+1));
    end
    j=find(mrec(2:end)~=mrec(1:end-1))+1;
    apd(i)=sum((mrec(j)-mrec(j-1)).*mpre(j));
    plot(rec,prec,col{i},'LineWidth',4);
    grid on;
    hold on;
end
%     plot(0:.1:1,0:.1:1,'r-','LineWidth',3);
%     classes{n_categories_+1} = 'EER';
xlabel('recall','FontSize',20)
ylabel('precision','FontSize',20)
title('Detection precision/recall curves.','Interpreter','none','FontSize',20);
legend(classes,'Interpreter','none','Location','SouthEast')
set(gca,'fontsize',20)
axis([0 1 0 1]);
apd = apd*100
saveFile =  fullfile(conf.tempSaveDir,'det_prec.png');
saveas(gcf,saveFile,'png');

%% classification precision-recall curve
if 0 %when the same file is used for testing all categories
    figure
    for i = 1:n_categories
        [foo,ind] = sort(prob(i,:),'descend');
        foo = zeros(1,length(score{i}));
        foo2 = zeros(1,length(score{i}));
        foo(find(score{i}(ind)>0.5)) = 1;
        foo2 = res(i,ind)==i;% images classified as i class
        %         foo = ones(1,length(score{i}));%excludes intersection threshold
        tp = foo.*gtS{i}.gt(ind).*foo2;
        fp = 1-tp;
        fp=cumsum(fp);
        tp=cumsum(tp);
        rec=tp/sum(gtS{i}.gt(ind)>0);
        prec=tp./(fp+tp);
        mrec=[0 ; rec' ; 1];
        mpre=[0 ; prec' ; 0];
        for j=numel(mpre)-1:-1:1
            mpre(j)=max(mpre(j),mpre(j+1));
        end
        j=find(mrec(2:end)~=mrec(1:end-1))+1;
        apc(i)=sum((mrec(j)-mrec(j-1)).*mpre(j));
        plot(rec,prec,'-','Color',col(i),'LineWidth',3);
        hold on
    end
    xlabel('recall','FontSize',15)
    ylabel('precision','FontSize',15)
    title('Classification precision/recall curves.','Interpreter','none','FontSize',15);
    legend(classes,'Interpreter','none','Location','SouthEast')
    set(gca,'fontsize',15)
    axis([0 1 0 1]);
    grid on
    apc
end

fprintf('   Done!\n');
end






