%O3D2D_CONST_FIND_PP_HYPOTHESES_RANSAC2 - Find hypotheses of
%                          object locations using the point pattern
%                          constellation model
%
% [hypS, bestScores] =
% o3d2d_const_find_pp_hypotheses_ransac2(evidLabels_,evidCoords_,ppModelS_,numOfHyps_,bboxFlag_,:)
%
% This function reads N best landmarks extracted from image and
% using the random sample consensus type search output M best
% hypotheses of object locations. The hypothesis selection is based
% on the probabilistic match score using the point pattern model
% and landmark likelihoods. For more information and detailed
% description read refs. [1,3].
%
% Output:
%
% Input:
%  evidLabels_ - Nx1 landmark labels 1,2,...
%  evidCoords_ - Nx2 coordinates of the labels (x,y)
%  ppModelS_   - Point pattern constellation model structure
%  numOfHyps_  - Number of retreived best hypothesis (def.10)
%  bboxFlag_   - Defines the use of bounding box corners in the mean
%                model (true/false)
%
%  <optional>
% 'ransac_iters'  - Number of random iterations (Def. 100)
% 'probRule'      - Probability rule to be used in the best
%                   hypothesis selection:
%                    'constsum' Sum of constellation likelihoods (Def.)
%                    'constsum_sc' Sum of constellation likelihood scores
%                    'constprod' Product of constellation likelihoods
%                    'lmsum' Sum of landmark likelihoods (does not make much sense)
%                    'constsum_lmsum_prod' product of the two sums
%                    'constsum_lmsum_prod2' same except for total score
% 'reestimRule'   - Probability rule to select landmarks to be re-estimated
%                    'const_lhood' Constellation likelihood (Def.)
%                    'const_sc' Constellation likelihood score
%                    'const_lhood_lm_lhood_prod' product of two
%
% 're_estim_const_lhood_th' - Likelihood limit for which outside
%                             the points are replaced with the mean
%                             model landmarks:
%                              'std1', 'std2' (def.), 'std3', 'std4'
% 're_estim_const_sc_th' -  Likelihood score limit for which
%                           outside the points are replaced with
%                           the mean model landmarks^*, e.g.,
%                            0.68 (def.), 0.95, 0.99, 0.999 which
%                            correspond std1, std2, etc.
% 'lm_lhoods'       - Needed by 'lm' related probRule and reestimRule
% 'lm_lhood_scores' - Needed by 'lm_sc' related probRule and resestimRule
% 'debugLevel'      - 0,1,2,3 (Def. 0)
% 'debugImg'        - Image used in the debug plots (Def. [])
%
% ^* Due to coarse approximation of score values this might produce
% slightly different results as compared with the std options, but
% this can be used with Gaussian mixture models as well.
%
% See also O3D2D_CONST_PPMODEL_FILES.M and
%          O3D2D_CONST_FIND_PP_HYPOTHESES_RANSAC.M .
%
% References:
%
%  [1] Kamarainen, J.-K., Supervised Learning of Probabilistic
%  Part-Based Models for Object Detection and Localisation, IEEE
%  PAMI 2012.
%
%  [2] Kamarainen, J.-K., Ilonen, J., Learning and Detection of
%  Object Landmarks in Canonical Object Space, 20th Int. Conf. on
%  Pattern Recognition (ICPR2010)  (Istanbul, Turkey, 2010).
%
%  [3] Object Localisation Using Generative Probability Model for
%  Spatial Constellation and Local Image Features
%  (J.-K. Kamarainen, M. Hamouz, J. Kittler, P. Paalanen,
%  J. Ilonen, A. Drobchenko), In ICCV Workshop on Non-Rigid
%  Registration and Tracking Through Learning, 2007.
%
%  [4] Gaussian mixture pdf in one-class classification: computing
%  and utilizing confidence values (J. Ilonen, P. Paalanen,
%  J.-K. Kamarainen, H. Kälviäinen), In 18th Int. Conf. on Pattern
%  Recognition (ICPR2006), 2006.
%
%  [5] Riabchenko, E., Kamarainen, J.-K., Chen, Ke, Density-Avare Part-Based
%  Object Detection with Positive Examples, 22th Int. Conf. on
%  Pattern Recognition (ICPR2014)  (Stockholm, Sweden, 2014)
%
% Author(s):
%    Joni Kamarainen <Joni.Kamarainen@lut.fi>
%    Ekaterina Riabchenko <Ekaterina.Riabchenko@lut.fi>%
% Copyright:
%
%   Object3Dto2D project 2D constellation model (*_const_*) toolbox is
%   Copyright (C) 2006-2012 by Joni-Kristian Kamarainen.
function [hypS, bestScores] = o3d2d_const_find_pp_hypotheses_ransac2(evidLabels_,evidCoords_,ppModelS_,numOfHyps_,bboxFlag_,varargin)
hypS = [];

% 1. Parse input arguments
conf = struct(...
    'ransac_iters', 100,...
    'probRule', 'combinatorical_score',...
    'reestimRule', 'const_lhood',...
    're_estim_const_lhood_th', 'std2',...
    're_estim_const_sc_th', 0.68,...
    'lm_lhoods', [],...
    'lm_lhood_scores', [],...
    'debugLevel', 0,...
    'tempSaveDir','.',...
    'debugImg', []);
conf = mvpr_getargs(conf,varargin);

% Resolve some features
if bboxFlag_
    numOfFeatureClasses = size(ppModelS_.meanModel,1) - 4;
else
    numOfFeatureClasses = size(ppModelS_.meanModel,1);
end
numOfCoordDims = size(ppModelS_.meanModel,2);

% Select min num of points for the selected homography
switch (ppModelS_.homography)
    case 'none',
        homogrPointsMin = 0;
    case 'isometry',
        homogrPointsMin = 2;
    case 'similarity',
        homogrPointsMin = 2;
    case 'affinity',
        homogrPointsMin = 3;
    case 'projectivity',
        homogrPointsMin = 4;
    otherwise,
        error(['Unknown ''homography'' set!']);
end;
rsMinFeatNum = homogrPointsMin;

% Preparing RANSAC loop variables
rsFeatCoords = evidCoords_;
rsFeatLabels = evidLabels_;

flag = 1;
k = 1;


% These are the overall best ones after loop max
bestScores = zeros(numOfHyps_,1); % tot score (probRule)
bestFeats = zeros(numOfHyps_,numOfFeatureClasses,numOfCoordDims);
bestFeatInds = zeros(numOfHyps_,numOfFeatureClasses);
bestLhoods = zeros(numOfHyps_,numOfFeatureClasses,1); % constellation
bestLhoodScores = zeros(numOfHyps_,numOfFeatureClasses,1);
bestLMLhoods = zeros(numOfHyps_,numOfFeatureClasses,1); % landmark
bestLMLhoodScores = zeros(numOfHyps_,numOfFeatureClasses,1);
bestConstScores = zeros(numOfHyps_,1);%constellation fit score

% try all pairs of candidates (to obtain constellation fit score)
comb = nchoosek(1:length(rsFeatLabels),2);
comb_save = [];
for i = 1:length(comb)
    if length(unique(rsFeatLabels(comb(i,:))))== 2
        comb_save = [comb_save; comb(i,:)];
    end
end

for j = 1:length(comb_save)
    ind = comb_save(j,:);
    badHyp=0;
    rsEstFeats = rsFeatCoords(ind,:);
    rsEstFeatsLabels = rsFeatLabels(ind,:);
    
    % 2. Compute homography using the selected points
    if (strcmp(ppModelS_.homography, 'none'))
        H = diag([1 1 1]);
    else
        H = mvpr_h2d_corresp(rsEstFeats', ppModelS_.meanModel(rsEstFeatsLabels,:)',...
            'hType', ppModelS_.homography);
    end;
    
    %transformation statistics checking
    if 1%flag
        try
            A = H(1:2,1:2);
            [U S V] = svd(A);
            s = S(1,1);
            t = H(1:2,3);
            R = U*V';
            %         det(R), pause
            if abs(det(R)-1)<10^(-5)
                if R(1,1)>0
                    alpha = asind(-R(1,2));
                else
                    alpha = 2*90*sign(asind(-R(1,2))) - asind(-R(1,2));
                end
            else
                error('error');
            end
            
            load(fullfile(conf.tempSaveDir, sprintf('transformStat_%s.mat',ppModelS_.category)));
            
        catch
            badHyp = badHyp + 1;
            if (sum(bestScores))&&(flag==1)
                flag = 0;
            end
            continue
        end
        
        if (alpha>transformS.r_max)||(alpha<transformS.r_min)||...
                (s>transformS.s_max)||(s<transformS.s_min)
            badHyp = badHyp + 1;
            if (sum(bestScores)==0)&&(flag==1)
                flag = 0;
            end
            continue;
        end
    end
    
    
    if (sum(isnan(H(:)))>0)
        warning('Homography estimation failed, using unity');
        H = diag([1 1 1]);
    end;
    % Transform all features to the model space
    TrsFeatCoords = mvpr_h2d_trans(rsFeatCoords',H)';
    %
    % 3. Select points providing the best scores (probRule)
    thisIterBestFeats = zeros(numOfFeatureClasses,numOfCoordDims);
    thisIterBestFeatInd = zeros(numOfFeatureClasses,1);
    thisIterBestLhoods = realmin*ones(numOfFeatureClasses,1);
    thisIterBestLmLhoods = realmin*ones(numOfFeatureClasses,1);
    for labelInd = 1:numOfFeatureClasses
        labelInds = find(rsFeatLabels == labelInd);
        if (strcmp('probRule', 'constsum_lmsum_prod'))
            TrsLhoods = conf.lm_lhoods.*gmmb_cmvnpdf(TrsFeatCoords(labelInds,:),...
                ppModelS_.localDistrGMMB(labelInd).mu',...
                ppModelS_.localDistrGMMB(labelInd).sigma);
        else
            TrsLhoods = gmmb_cmvnpdf(TrsFeatCoords(labelInds,:),...
                ppModelS_.localDistrGMMB(labelInd).mu',...
                ppModelS_.localDistrGMMB(labelInd).sigma);
        end;
        [maxLhood maxInd] = max(TrsLhoods);
        thisIterBestFeats(labelInd,:) = rsFeatCoords(labelInds(maxInd),:);
        thisIterBestFeatInd(labelInd) = labelInds(maxInd);
        thisIterBestLhoods(labelInd) = maxLhood;% constellation lhoods
        thisIterBestLmLhoods(labelInd) = conf.lm_lhoods(labelInds(maxInd));
    end;
    
    % forming a score (p) for a hypothesis
    inds = [1:numOfFeatureClasses];
    tmp = (thisIterBestLhoods.*thisIterBestLmLhoods);
    for i = 1:numOfFeatureClasses
        combInd = nchoosek(inds,i);
        tmp1 = reshape(thisIterBestLhoods(combInd),size(combInd));
        lhoodOfMatchedEvids(i) = sum(prod(tmp1,2));
        tmp2 = reshape(thisIterBestLmLhoods(combInd),size(combInd));
        lhoodOfMatchedLms(i) = sum(prod(tmp2,2));
        tmp3 = reshape(tmp(combInd),size(combInd));
        lmLhood(i) = sum(prod(tmp3,2));
    end
    tmp1(rsEstFeatsLabels) = 1;% lhoods of the two lms used for evaluation of the constellation fit are excluded
    p = prod(tmp1)*...
        sum(lhoodOfMatchedLms(1:end));
    
    %%
    %
    % 4. Compute this iter overall likelihood and update the candidate list
    switch conf.probRule
        case 'constsum',
            probScore = sum(thisIterBestLhoods);
        case 'constsum_sc',
            probScore = sum(1-diag(gmmb_lhood2frac(ppModelS_.localDistrGMMBHistS,repmat(thisIterBestLhoods,[1 numOfFeatureClasses]))));
        case 'constprod',
            probScore = prod(thisIterBestLhoods);
        case 'lmsum',
            probScore = sum(thisIterBestLmLhoods);
        case 'lmsum_sc',
            error(['Not implemented since coarse likelihood score makes ' ...
                'scores equal to zero - use likelihoods instead!']);
        case {'constsum_lmsum_prod','constsum_lmsum_prod2'},
            [sum(thisIterBestLhoods) log(sum(thisIterBestLmLhoods))]
            probScore = sum(thisIterBestLhoods)*sum(thisIterBestLmLhoods);
        case 'combinatorical_score',
            probScore = p;
        case 'new',
            probScore = (sum(thisIterBestLhoods))*exp(prod(thisIterBestLmLhoods./max(thisIterBestLmLhoods)));
        otherwise,
            error('Unimplemented probability rule!');
    end;
    % skip if this hypothesis already included
    if ( sum(sum((bestFeats-repmat(shiftdim(thisIterBestFeats,-1),...
            [numOfHyps_ 1 1])).^2,2) == 0))
        continue;
    end;
    firstWorseInd = find(probScore > bestScores);
    if (~isempty(firstWorseInd))
        bestScores(firstWorseInd(1)+1:end) = bestScores(firstWorseInd(1):end-1);
        bestScores(firstWorseInd(1)) = probScore;
        
        bestFeats(firstWorseInd(1)+1:end,:,:) = bestFeats(firstWorseInd(1):end-1,:,:);
        bestFeats(firstWorseInd(1),:,:) = thisIterBestFeats;
        
        bestFeatInds(firstWorseInd(1)+1:end,:) = bestFeatInds(firstWorseInd(1):end-1,:);
        bestFeatInds(firstWorseInd(1),:,:) = thisIterBestFeatInd;
        
        bestLhoods(firstWorseInd(1)+1:end,:) = bestLhoods(firstWorseInd(1):end-1,:);
        bestLhoods(firstWorseInd(1),:) = thisIterBestLhoods;
        
        bestLhoodScores(firstWorseInd(1)+1:end,:) = bestLhoodScores(firstWorseInd(1):end-1,:);
        bestLhoodScores(firstWorseInd(1),:) = 1-diag(gmmb_lhood2frac(ppModelS_.localDistrGMMBHistS,repmat(thisIterBestLhoods,[1 numOfFeatureClasses])));
        
        bestLMLhoods(firstWorseInd(1)+1:end,:) = bestLMLhoods(firstWorseInd(1):end-1,:);
        bestLMLhoods(firstWorseInd(1),:) = thisIterBestLmLhoods;
        
    end;
    
end;

%
% Re-estimate landmarks for the ones with too low pdf
if (strcmp(conf.reestimRule, 'const_lhood'))
    switch conf.re_estim_const_lhood_th
        case 'std1',
            lhoodLimits = ppModelS_.std1pdf;
        case 'std2',
            lhoodLimits = ppModelS_.std2pdf;
        case 'std3',
            lhoodLimits = ppModelS_.std3pdf;
        otherwise,
            error('Unknown likelihood limit rule!');
    end;
elseif (strcmp(conf.reestimRule, 're_estim_const_sc_th'))
    lhoodLimits = conf.re_estim_sc;
elseif (strcmp(conf.reestimRule, 'const_lhood_lm_lhood_prod'))
    error('unimplemented since limit difficult to set');
end;

bestLhoodsMask = zeros(size(bestLhoods));
for lmInd = 1:numOfFeatureClasses
    switch conf.reestimRule
        case 'const_lhood',
            bestLhoodsMask(:,lmInd) = (bestLhoods(:,lmInd)- ...
                lhoodLimits(numOfFeatureClasses) < 0);
        case 'const_sc',
            bestLhoodsMask(:,lmInd) = (bestLhoodScores(:,lmInd)-lhoodLimits < 0);
            
        otherwise,
            error('Unimplemented probability rule!');
    end;
end;

% Transform model points to the detected points (excluding poor
% likelihood ones)
reBestFeats = bestFeats;
if bboxFlag_
    badHyp = 1:size(reBestFeats,1);
    numOfIter = length(badHyp);
    bestLhoodsMask = [bestLhoodsMask ones(size(bestLhoodsMask,1),4)];
else
    badHyp = find(sum(bestLhoodsMask,2) > 0)'
    numOfIter = length(badHyp);
end
for cnt = 1:numOfIter
    hypInd = badHyp(cnt);
    corrInds = find(squeeze(bestLhoodsMask(hypInd,:)) == 0);
    badInds = find(squeeze(bestLhoodsMask(hypInd,:)) ~= 0);
    if (length(corrInds) < homogrPointsMin)
        continue; % not enough points for estimation
    end;
    if (strcmp(ppModelS_.homography, 'none'))
        H = diag([1 1 1]);
    else
        H = mvpr_h2d_corresp(ppModelS_.meanModel(corrInds,:)',...
            squeeze(bestFeats(hypInd,corrInds,:))',...
            'hType', ppModelS_.homography);
    end;
    modelCoords = mvpr_h2d_trans(ppModelS_.meanModel',H)';
    reBestFeats(hypInd,badInds,:) = modelCoords(badInds,:);
end;

hypS = reBestFeats;

