% WRITE_GT - vector with length of the test set, where 1 stands for images 
%            of the class being processed (true class)
%
%  [] = write_gt(class_, mode_, listFile_,:)
%
% Output:
%
% Input:
%  class_              - The name of the object ctaegory
%  mode_               - Postfix for path (train/test)
%  listFile_           _ File with full path containing images
% 
%  <optional>
% 'listFile'    - Image list file name used if listFile_ empty
%                   (Def. 'validatetraindata_accepted.txt')
% 'tempSaveDir'   - Output directory for all produced files
%                   (Def: '/tmp') - Note that you should specify
%                   this carefully as the system temp is cleaned
%                   periodically
%
% Author(s):
%    Ekaterina Riabchenko <Ekaterina.Riabchenko@lut.fi>
% 

function [] = write_gt(class_, mode_, listFile_, varargin)

conf = struct(...
    'listFile', 'validatetraindata_accepted.txt',...
    'tempSaveDir', '/tmp');
conf = mvpr_getargs(conf,varargin);

% make save dir if not exist
        if (~isdir(conf.tempSaveDir))
             mkdir(conf.tempSaveDir);
        end;
        
if strmatch(mode_,'test')
    gtSaveFile = fullfile(conf.tempSaveDir,sprintf('groundtruth_%s.mat',class_));
else
    gtSaveFile = fullfile(conf.tempSaveDir,sprintf('groundtruth_%s_%s.mat',class_,mode_));
end
fr = mvpr_lopen(listFile_, 'read');
filepair = mvpr_lread(fr);
i = 1;
while ~isempty(filepair)
    if ~isempty(strfind(filepair{1},class_))
        gt(i) = 1;
    else
        gt(i) = 0;
    end
    i = i+1;
    filepair = mvpr_lread(fr);
end
save(gtSaveFile, 'gt');
mvpr_lclose(fr);
