%O3D2D_EVEX_EXTRACTTRAINFEATURES - Extract Gabor features at
%                                  landmark locations and store
%                                  into matrix
%
%
% This function requirest a list of images and their landmark
% files. It constructs the canonical object space [1,2] and
% extracts multi-resolution Gabor features at the landmark
% locations from each image transformed to the canonical object
% space. Extracted features are stored to Ptrain with corresponding
% "labels" (landmark numbers) in Ttrain.
%
% NOTE: Prior to this it is recommended to run
% EVEX_VALIDATETRAINDATA.M which makes sure that your data is ok.
%
% Output:
%  Ptrain - Sample matrix containing extracted features with
%           different filters on different column and different
%           landmarks on different rows
%  Ttrain - Target matrix containing the correct landmark number
%           for each row in Ptrain
%
% Input:
%  trainListFile_ - File with full path containing training images
%                   and landmark files (if empty, then
%                   'reportFile' read from the 'tempSaveDir')
%  bboxFlag_      - defines the use of bounding box corners in the mean
%                   model (true/false)
%  genLmsFlag_    - used to chnge paths, when automatically generated
%                   landmarks are used
%  cls_           - name of the category of images
%
%  <optional>
%  gabor_fmax     - Maximum (base) Gabor bank frequency [1,2]
%                   (typically in {1/20,1/30,1/40,1/50}, def. 1/20)
%  gabor_fnum     - Number of frequencies (typically in {3,4,6},
%                   def. 4) [1,2]
%  gabor_thetanum - Number of orientations (typically in {4,6,8},
%                   def. 6) [1,2]
%  gabor_k_       - The frequency scaling factor - starting from the
%                   base frequency (typically in
%                   {sqrt(2),sqrt(3),sqrt(4)}, def. sqrt(3)) [1,2]
% 'gabor_p'       - Gabor filter envelope overlap [0,1] (def. 0.65)
% 'homography'    - {'none','isometry','similarity','affinity','projectivity'}
%                   (Def. 'similarity'), see [1,2]
% 'reportFile'    - Image list file name (Def. 'validatetraindata_accepted.txt')
% 'tempSaveDir'   - Output directory for all produced files
%                   (Def: '/tmp') - Note that you should specify
%                   this carefully as the system temp is cleaned
%                   periodically
% 'debugLevel'    - 0,.. (Def. 0)
% 'trainSaveFile' - Ouput file (where Ptrain and Ttrain saved)
%                   (Def. 'extracttrainfeatures_trainfeatures.mat')%
% 'imageRoot'     - '.'
% 'landmarkRoot'  - '.'
%
% See also O3D2D_EVEX_VALIDATETRAINFEATURES.M and
%          O3D2D_EVEX_TRAINFEATURECLASSIFIER.M .
%
% References:
%
%  [1] Kamarainen, J.-K., A revised simple Gabor feature space:
%  supervised local part detector, Image and Vision Computing,
%  2011.
%
%  [2] Kamarainen, J.-K., Ilonen, J., Learning and Detection of
%  Object Landmarks in Canonical Object Space, 20th Int. Conf. on
%  Pattern Recognition (ICPR2010)  (Istanbul, Turkey, 2010).
%
% Author(s):
%    Joni Kamarainen <Joni.Kamarainen@lut.fi>
%    Jarmo Ilonen <Jarmo.Ilonen@lut.fi>
%
% Copyright:
%
%   Part detector (evidence extraction, *_evex_*) toolbox is
%   Copyright (C) 2008-2010 by Joni-Kristian Kamarainen and Jarmo
%   Ilonen.
%
function [Ptrain,Ttrain] = o3d2d_evex_extracttrainfeatures_files(bboxFlag_,genLmsFlag_,cls_,trainListFile_,varargin);

% 1. Parse input arguments
conf = struct(...
    'gabor_fmax', 1/20,...
    'gabor_fnum', 4,...
    'gabor_thetanum', 6,...
    'gabor_k', sqrt(3),...
    'gabor_p', 0.65,...
    'homography', 'similarity',...
    'reportFile', 'validatetraindata_accepted.txt',...
    'tempSaveDir', '/tmp',...
    'debugLevel', 0,...
    'trainSaveFile', 'extracttrainfeatures_trainfeatures.mat',...
    'imageRoot', '.',...
    'landmarkRoot', '.');
conf = mvpr_getargs(conf,varargin);

% 2. Form the mean model space (using landmarks) which is used to
% normalise images before extracting the features
fprintf('   Forming the mean model space for image normalisation...\n');

if (isempty(trainListFile_))
    [meanModel meanModelErr mmLMarks] = ...
        o3d2d_evex_meanmodel_files(fullfile(conf.tempSaveDir,conf.reportFile),...
        bboxFlag_,...
        genLmsFlag_,...
        cls_,...
        conf.homography,...
        'landmarkRoot', conf.landmarkRoot,...
        'debugLevel', conf.debugLevel,...
        'forceSeed',true);
else
    [meanModel meanModelErr mmLMarks] = ...
        o3d2d_evex_meanmodel_files(trainListFile_,...
        bboxFlag_,...
        genLmsFlag_,...
        cls_,...
        conf.homography,...
        'landmarkRoot', conf.landmarkRoot,...
        'debugLevel', conf.debugLevel,...
        'forceSeed',true);
end;

fprintf('Done (err = %4.2f pixels)!!\n',meanModelErr);



% 3. Go through all valid images one by one and extract features from
% them


% Re-open file that contains image and position files
fh = mvpr_lopen(fullfile(conf.tempSaveDir,conf.reportFile), 'read');

% read image and position file names
filepair = mvpr_lread(fh);

% Go through all valid images and extract features
trainFeatures = [];
imgInd = 0;

Ptrain = [];
Ttrain = [];

fprintf('   Extracting Gabor features...\n');
while ~isempty(filepair)
    imgInd = imgInd+1;
    fprintf(['\r   Feature extraction: image number %d'], imgInd);
    
    % read image and evidence locations
    gimg = mvpr_imread(fullfile(conf.imageRoot,filepair{1}), 'range', [0 1], 'type', 'double');
    evidPos = load(fullfile(conf.landmarkRoot,filepair{2}));
    
    %%%%%%%%%%%%%%%%%% DEBUG[2] %%%%%%%%%%%%%%%%%%%
    if (conf.debugLevel >= 3)
        imagesc(gimg);
        colormap(gray);
        axis off;
        hold on;
        for dbgInd2 = 1:size(evidPos,1)
            text(evidPos(dbgInd2,1),...
                evidPos(dbgInd2,2),...
                num2str(dbgInd2),...
                'FontSize',18,'Color','black',...
                'BackgroundColor','white','EdgeColor','black');
        end;
        hold off;
        drawnow;
        if (conf.debugLevel >= 2)
            input('Debug[2]: Original image and evidences <RETURN>');
        end;
    end;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Normalise the image
    H = mvpr_h2d_corresp(evidPos',meanModel','hType',conf.homography);
    try
        [Tgimg TH] = mvpr_imtrans(gimg,H,'pxlimit',2000000);
    catch
        warning('evex_extracttrainfeatures:imtransFailed',...
            'h2d_imtrans failed.. skipping this image!');
        % must read next line or this starts inf loop
        filepair = mvpr_lread(fh);
        continue;
    end;
    TevidPos = mvpr_h2d_trans(evidPos',TH)'; % remap as image
    % trasform changes image
    
    %%%%%%%%%%%%%%%%%% DEBUG[2] %%%%%%%%%%%%%%%%%%%
    if (conf.debugLevel >= 3)
        imagesc(Tgimg);
        axis off;
        colormap(gray);
        hold on;
        for dbgInd2 = 1:size(evidPos,1)
            text(TevidPos(dbgInd2,1),...
                TevidPos(dbgInd2,2),...
                num2str(dbgInd2),...
                'FontSize',18,'Color','black',...
                'BackgroundColor','white','EdgeColor','black');
        end;
        hold off;
        drawnow
        if (conf.debugLevel >= 2)
            input('Debug[2]: Aligned and normalized image and evidences <RETURN>');
        end;
    end;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % set NaN's to zero
    Tgimg(isnan(Tgimg)) = 0;
    
    % Create Gabor filter bank of given image size
    %  * Slow computation does not harm with the training set
    warning('off','sg_createfilterbank:largeDimensionFactor');
    gaborBank = mvpr_sg_createfilterbank(...
        size(Tgimg),...
        conf.gabor_fmax,...
        conf.gabor_fnum,...
        conf.gabor_thetanum,...
        'pf',0.9999,...
        'k',conf.gabor_k,...
        'p',conf.gabor_p);
    %    'gamma',1,...
    %    'eta',1);
    warning('on','sg_createfilterbank:largeDimensionFactor');
    
    %%%%%%%%%%%%%%%%%% DEBUG[2] %%%%%%%%%%%%%%%%%%%
    if (conf.debugLevel >= 2 & imgInd == 1)
        for dbgInd1 = 1:length(gaborBank.freq)
            foo1 = mvpr_sg_createfilterf2(gaborBank.freq{dbgInd1}.f,0,...
                gaborBank.conf.gamma,...
                gaborBank.conf.eta,size(Tgimg));
            dbgImg = zeros(size(Tgimg));
            dbgImg(round(TevidPos(2,2)),round(TevidPos(2,1))) = 1;
            dbgImg = conv2(dbgImg,...
                abs(fftshift(ifft2(foo1))),'same');
            imagesc(dbgImg.*Tgimg);
            axis off;
            input(['Debug[2]: Effective area of Gabor filter at ' ...
                'frequency ' num2str(gaborBank.freq{dbgInd1}.f) '<RETURN>']);
        end;
    end;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Calculate Gabor features using the Gabor bank
    if (max(TevidPos(:,1)) > size(Tgimg,2) ||...
            max(TevidPos(:,2)) > size(Tgimg,1) ||...
            min(TevidPos(:,1)) < 1 ||...
            min(TevidPos(:,2)) < 1 )
        warning('Evidence point outside the image - omitted this image!');
    else
        G = mvpr_sg_filterwithbank(Tgimg,gaborBank,'points',round(TevidPos),...
            'method',1,'max_zoom',1);
        featVect = mvpr_sg_resp2samplematrix(G)./prod(G.actualZoom);
        labelVect = [1:size(evidPos,1)]';
        
        Ptrain = [Ptrain; featVect];
        Ttrain = [Ttrain; labelVect];
    end;
    
    % Save train data (responses at evidences) approx. 10% of iters
    if (rand() <= 0.1)
        fprintf(' [Random saving to ''trainSaveFile'']\n');
        save(fullfile(conf.tempSaveDir,conf.trainSaveFile),...
            'Ptrain','Ttrain');
    end;
    
    % read image and position file names
    filepair = mvpr_lread(fh);
end;
fprintf('\n   Done!\n');
save(fullfile(conf.tempSaveDir,conf.trainSaveFile),...
    'Ptrain','Ttrain');

mvpr_lclose(fh);

fprintf('   Extracted training features saved to ''%s'' (vars: Ptrain,Ttrain)\n', ...
    fullfile(conf.tempSaveDir,conf.trainSaveFile));
