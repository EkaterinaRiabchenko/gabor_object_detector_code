/*
  See uptodate docs in evex_heapupdate.m

  evex_heapupdate(heap, image, shiftParams)
  evex_heapupdate(heap,'sort')

  Compilation: mex evex_heapupdate.c
  The mex-command can be found in default matlab/bin directory, 
  like 'matlab'.

  Author:
    Jarmo Ilonen, <ilonen@lut.fi>, 2005

  $Id: evex_heapupdate.c,v 1.1 2008/09/16 10:19:19 jkamarai Exp $

*/

#include <math.h>
#include <string.h>
#include "mex.h"
#include "matrix.h"

#define USAGESTRING "Usage: evex_heapupdate(heap, image, shiftParams) or\n       evex_updateheap(heap,'sort')\n"

/* swaps two elements in the heap */
void swapHeap(int i, int father, int dElements, int dElementSize, double * dHeap, double * tmp) 
{
  tmp=memcpy(tmp,dHeap + father * dElements, dElements*dElementSize);
  memcpy(dHeap + father * dElements, dHeap + i * dElements, dElements*dElementSize);
  memcpy(dHeap + i * dElements, tmp, dElements*dElementSize);
}

void createElement(int element, double value, int x, int y, int dElements, int dElementSize, double * dHeap, int bElements, double * bData)
{
  /* put the value into heap */
  dHeap[element*dElements + 0] = value;
  /* and the coordinates */
  dHeap[element*dElements + 1] = (double)x;
  dHeap[element*dElements + 2] = (double)y;
  /* and bula */
  if (bElements>0) 
    memcpy(dHeap + element*dElements + 3, bData, bElements*dElementSize);
}

void liftElement(int element, int dElements, int dElementSize, double * dHeap, int maxn, double * tmp)
{
  int i,father;

  /* lift the element to the right place */
  i=element;
  father=(int)( (i-1)/2 );
  
  while (father>=0 && dHeap[father * dElements] > dHeap[i * dElements]) 
  {
    /* swap the places */
    swapHeap(i,father,dElements,dElementSize,dHeap, tmp);
    
    i=father;
    father=(int)( (i-1)/2 );
    
  }
}

void dropElement(int element, int dElements, int dElementSize, double * dHeap, int maxn, double * tmp)
{
  int i,l1,l2;
  /* drop the element to the right place */
  
  i=0;
  
  while ( (i+1)*2-1 < maxn ) 
  {
    /* get the children */
    l1=(i+1)*2-1;
    l2=(i+1)*2;
    
    if (l2==maxn) {
      /* maxn must be even (meaning there is only one child), so there is only
       * l1 and l2 points past the end of all */
      l2=l1; /* stupid kludge but I'm lazy */
    }

    /* if they are both larger then the correct place has been found */
    if (dHeap[i * dElements] < dHeap[l1 * dElements] && 
	dHeap[i * dElements] < dHeap[l2 * dElements] ) 
      break;
    
#ifdef DEBUG
    printf("Dropping from %d... ", i);
#endif

    /* drop the current element to the place of smaller of the children */
    if (dHeap[l1 * dElements] < dHeap[l2 * dElements]) 
    {
      swapHeap(i,l1,dElements,dElementSize,dHeap, tmp);
#ifdef DEBUG
      printf("to %d\n",l1);
#endif
      i=l1;
    } else {
      swapHeap(i,l2,dElements,dElementSize,dHeap, tmp);
#ifdef DEBUG
      printf("to %d\n",l2);
#endif
      i=l2;
    }
    
  }
  
}

/* updates largest value from image to the heap */
int updateHeap(int n, int maxn, int dElements, int dElementSize, double * dHeap, int x0, int y0,double * dImage, int bElements, double * bData)
{
  int x,y;

  int heapfull=0;

  /* temp space for swapping elements */
  double * tmp;
  tmp=(double * )mxCalloc(dElements,dElementSize);

  if (n==maxn)
    heapfull=1;

  /* loop for going through the image pixel by pixel */
  for (x=0;x<x0;x++) 
  {
    for (y=0;y<y0;y++) 
    {

      /* heap not full */
      if (!heapfull) 
      {
	/* place new element to the last place of the heap */
	createElement(n, dImage[x*y0 + y], x, y, 
		      dElements, dElementSize, dHeap, bElements, bData);

	liftElement(n,dElements, dElementSize,dHeap,maxn,tmp);
	
	n++;
	if (n==maxn) 
	  heapfull=1;
	
      } else {
	/* heap fully populated */
	
	if (dImage[x*y0 + y] > dHeap[0]) 
	{
	  /* it's larger yep yep yep, replace it */
	  createElement(0, dImage[x*y0 + y], x, y, 
			dElements, dElementSize, dHeap, bElements, bData);
	  
	  /* drop the element to the right place */
	  dropElement(0,dElements, dElementSize,dHeap,maxn,tmp);

	}
      }
    }
  }
  mxFree(tmp);

  return n;
}

int sortHeap(int n, int maxn, int dElements, int dElementSize, double * dHeap)
{
  double * tmp;
  tmp=(double * )mxCalloc(dElements,dElementSize);

  n--;

  while (n>0) 
  {
    /* the smallest element is at 0, swap it with the last element */
#ifdef DEBUG
    printf("Swapping elements %d and %d\n",0,n);
#endif
    swapHeap(0,n,dElements,dElementSize,dHeap, tmp);
    /* drop the current element to its right place */
    dropElement(0,dElements, dElementSize,dHeap,n,tmp);
    n--;
  }
  mxFree(tmp);
  return n;
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs,
                 const mxArray *prhs[])
{
  mxArray    * tmpArrayPtr;
  double     * tmpDoublePtr;

  double     * nDoublePtr;
  mxArray    * v;
  double     * vData;

  int n, maxn;
  int dElements,dElementSize;

  double     * imageData;
  double     * bulaData;
  int        bulaLen;

  int action=0;

  /* Check proper input and output */
  
  /* if we have only two parameters, we must be sorting the heap */
  if (nrhs==2) 
  {
    char str[80];
    /* second parameter must be string equivalent to "sort" */
    if (! mxIsChar(prhs[1])) {
      printf("Invalid arguments:\n%s\n",USAGESTRING);
      return;
    }
    
    mxGetString(prhs[1], str, 80);

    if (strncmp(str,"sort",80)==0) 
    {
      action=2;
    } else {
      printf("WTF?\n");
      return;
    }
  }

  /* if its something else than three arguments, then pls stfu kthx */
  if (nrhs==3) 
  {
    action=1;
  }

  /* no sensible arguments */
  if (action==0) 
  {      
    printf("Invalid arguments:\n%s\n",USAGESTRING);
    return;
  }
  
  /* the first argument better be the heap structure */
  if (!mxIsStruct(prhs[0]))
    mexErrMsgTxt("Input must be a structure.");

  /* parse n */
  tmpArrayPtr = mxGetField(prhs[0],0,"n");
  nDoublePtr = mxGetPr(tmpArrayPtr);
  n=(int)(nDoublePtr[0]);

  /* parse maxn */
  tmpArrayPtr = mxGetField(prhs[0],0,"maxn");
  tmpDoublePtr = mxGetPr(tmpArrayPtr);
  maxn=(int)(tmpDoublePtr[0]);

  /* get v */
  v = mxGetField(prhs[0],0,"v");
  vData=mxGetPr(v);
  
  /* number of elements and elementsize */
  dElements=mxGetM(v);
  dElementSize=mxGetElementSize(v);

  /* sort  */
  if (action==2) 
  {
    n=sortHeap(n, maxn, dElements, dElementSize, vData);
  }

  /* update heap */
  if (action==1) 
  {
    /* get pointer to bula */
    bulaData=mxGetPr(prhs[2]);
    bulaLen=mxGetN(prhs[2]);
    
    /* get image */
    imageData=mxGetPr(prhs[1]);
    
    
    if (mxGetElementSize(v) != mxGetElementSize(prhs[2])) {
      printf("Woe to you oh earth and sea for the devil sends the beast with wrath because he knows the time is short.\n");
      return;
    }
    
    /* update heap */
    n=updateHeap(n,maxn,dElements,dElementSize,vData,
		 mxGetN(prhs[1]),mxGetM(prhs[1]),imageData,
		 bulaLen, bulaData);
    
  }

  /* update n to the input structure */
  nDoublePtr[0]=(double)n;

  return;
}

