%O3D2D_EVEX_VALIDATETRAINDATA_FILES - Validate training data
%                                     entries
%
%
% This function loads every training image and their landmarks
% listed in trainListFile_ (one image and landmark file name per
% line). Validation results are returned in a struct (validS)
% and also saved to a file (reportFile), which can also be used to
% process only the passed entries.
%
% NOTE: The first entry line should represent the correct training
% data (e.g. the correct and desired number of landmarks) - others
% are compared and possibly NOT accepted as compared to the first
% one. 
%
% NOTE: This function can be made DATASET SPECIFIC - make a copy
% with another name or add a dataset specific sub function to this
% file! The purpose of this function is to check available training
% data in all possible ways the user wants and then produce a list
% of files that have passed the inspection and can be really used
% in the generations of training features and a classifier. Can be
% used, for example, to filter out too small or too large images!
%
% Output:
%  validS - Struct containing validation statistics
%
% Input:
%  trainListFile_ - File with full path containing training images
%                   and landmark files
%  genLmFlag_ -  is used if automatically generated landmarks are used
%
% <optional>
% 'imageRoot'    - '.'
% 'landmarkRoot' - '.'
% 'reportFile'   - Output file name (Def: 'validatetraindata_accepted.txt')
% 'tempSaveDir'  - Output directory for all produced files
%                  (Def: '/tmp') - Note that you should specify
%                  this carefully as the system temp is cleaned
%                  periodically
% 'debugLevel'   - 0,.. (Def. 0)
%
% See also MVPR_EVEX_EXTRACTTRAINFEATURES.M .
%
% References:
%
%  [1] Kamarainen, J.-K., A revised simple Gabor feature space:
%  supervised local part detector, Image and Vision Computing,
%  2011.
%
% Author(s):
%    Joni Kamarainen <Joni.Kamarainen@tut.fi>
%    Jarmo Ilonen <Jarmo.Ilonen@lut.fi>
%
% Copyright:
%
%   Part detector (evidence extraction, *_evex_*) toolbox is
%   Copyright (C) 2008-2010 by Joni-Kristian Kamarainen and Jarmo
%   Ilonen.
%
function [S] = o3d2d_evex_validatetraindata_files(genLmFlag_,trainListFile_,varargin);

% Parse input arguments
conf = struct('reportFile', 'validatetraindata_accepted.txt',...
              'imageRoot', '.',...
              'landmarkRoot', '.',...
              'tempSaveDir', '/tmp',...
              'debugLevel', 0);
conf = mvpr_getargs(conf,varargin);


%
% Run data set specific evaluation function
S = evaltraindatageneral(genLmFlag_,trainListFile_,conf.imageRoot,conf.landmarkRoot);
S.reportFile = conf.reportFile;

%
% Write training set statistics to file

% some statistics vectors
ngood = length(S.passed);

% save good image info
if (~exist(conf.tempSaveDir,'dir'))
    mkdir(conf.tempSaveDir)
end;
fh = mvpr_lopen(fullfile(conf.tempSaveDir,conf.reportFile), 'write');

% Print stdout
fprintf('--------- Statistics ------------\n');
fprintf([' Contains ' num2str(ngood) ' accepted training images out of '...
                 num2str(length(S.passed)+length(S.failed)) ' images in total.\n']);
fprintf(' Image properties:\n');
fprintf(['   evidences: ' num2str(S.numOfEvidences) '\n']);
fprintf(['   width range:  ' mat2str(S.Xrange) '\n']);
fprintf(['   height range: ' mat2str(S.Yrange) '\n']);
fprintf(['   aspect ratio (width/height) range: ' mat2str(S.aspRatRange,4) '\n']);
fprintf(['   evidence dist (first, last) range: ' mat2str(S.evidDistRange,4) '\n']);
fprintf(['   normalised evidence dist (by diagonal) range: ' mat2str(S.normEvidDistRange,4) '\n']);
fprintf(' Normalization evidence statistics:\n');
fprintf('   - None recorded (not needed anymore)- \n');
fprintf('---------------------------------\n');

% Print to file (as comments)
mvpr_lwritecomment(fh, [' Contains ' num2str(ngood) ' training images.']);
mvpr_lwritecomment(fh,  ' Image properties:');
mvpr_lwritecomment(fh, ['   evidences: ' num2str(S.numOfEvidences)]);
mvpr_lwritecomment(fh, ['   width range:  ' mat2str(S.Xrange)]);
mvpr_lwritecomment(fh, ['   height range: ' mat2str(S.Yrange)]);
mvpr_lwritecomment(fh, ['   aspect ratio (width/height) range: ' mat2str(S.aspRatRange,4)]);
mvpr_lwritecomment(fh, ['   evidence dist (first, last) range: ' mat2str(S.evidDistRange,4)]);
mvpr_lwritecomment(fh, ['   normalised evidence dist (by diagonal) range: ' mat2str(S.normEvidDistRange,4)]);
mvpr_lwritecomment(fh,  ' Normalization evidence statistics:');
mvpr_lwritecomment(fh,  '   - None recorded (not needed anymore)- ');

for i = 1:ngood
	mvpr_lwrite(fh, {S.passed(i).img_fname, S.passed(i).pos_fname});
end

mvpr_lclose(fh);
fprintf(['  Statistics written to ''' ...
         fullfile(conf.tempSaveDir,conf.reportFile) ...
         '''\n']);

%%%%%%%%%% DEBUG[2] %%%%%%%%%%%%%%%%%%%%%%
if (conf.debugLevel >= 1)
  hist(S.evidDists);
  input('Debug[2]: Distribution of the first and last landmark distances <RETURN>');
  hist(S.normEvidDists);
  input('Debug[2]: Distribution of the first and last landmark distances <RETURN>');
end;
%%%%%%%%%%%%%%%%%%%%%%%%%c%%%%%%%%%%%%%%%%%


% -----------------------------------------------------------------------------

%
% EVALTRAINDATAGENERAL data set specific data evaluation
function [S] = evaltraindatageneral(genLmFlag_,trainImgListFile_,imgRoot_,lmRoot_)

% Process the whole list of training data, discard invalid items.
tryparams = struct(...
	'numOfEvidences', 0);

numOfImgs = 0; % total number of images

% good images
imagelist = struct(...
	'img_fname', {}, ...
	'pos_fname', {});

% bad images
failedlist = struct(...
	'img_fname', {}, ...
	'pos_fname', {} );


% minimum and maximum sizes of images
maxX = -inf;
maxY = -inf;
maxAspRat = -inf;
maxEvidDist = -inf;
maxNormEvidDist = -inf;
evidDists = [];
normEvidDists = [];
minX = inf;
minY = inf;
minAspRat = inf;
minEvidDist = inf;
minNormEvidDist = inf;


% open file that contains image and position files
% on a line: 1st item is imgFile, 2nd item is posFile
fh = mvpr_lopen(trainImgListFile_, 'read');

% read first ones to inspect them
filepair = mvpr_lread(fh);

if genLmFlag_
    [lmDir lmFile ext] = fileparts(filepair{2});
    lmSaveFile = fullfile(lmRoot_,['Generated_' lmDir],[lmFile ext]);
else
   lmSaveFile = fullfile(lmRoot_,filepair{2});
end

% Take correct number of evidences from the first
try 
    evidPos = load(lmSaveFile);
catch
    error(['Cannot read the first entry landmark file: ' ...
          lmSaveFile '  - needed for defaults!']);
end;
numOfEvidences = size(evidPos,1);
tryparams.numOfEvidences = numOfEvidences;

% Go through all image and position files and evaluate them
while ~isempty(filepair)
	numOfImgs = numOfImgs+1; % total number of files in list file
	fprintf('\r  Image: %5d',numOfImgs);
	
	failed = 0;
	info = try_image(fullfile(imgRoot_,filepair{1}), lmSaveFile, tryparams);
	
	if isempty(info)
		% training data item was somehow bad
		i = length(failedlist)+1;
		failedlist(i).img_fname = filepair{1};
        if genLmFlag_
          failedlist(i).pos_fname = ['Generated_' filepair{2}];  
        else
		  failedlist(i).pos_fname = filepair{2};
        end
	else
		% data item was good
		minX = min(minX, info.width);
		minY = min(minY, info.height);
                minAspRat = min(minAspRat,info.width/info.height);
                minEvidDist = min(minEvidDist,info.evidDist);
                minNormEvidDist = min(minNormEvidDist,info.normEvidDist);
		maxX = max(maxX, info.width);
		maxY = max(maxY, info.height);
                maxAspRat = max(minAspRat,info.width/info.height);
                maxEvidDist = max(minEvidDist,info.evidDist);
                maxNormEvidDist = max(minNormEvidDist,info.normEvidDist);
		i = length(imagelist)+1;
                evidDists(i) = info.evidDist;
                normEvidDists(i) = info.normEvidDist;
		imagelist(i).img_fname = filepair{1};
        if genLmFlag_
          imagelist(i).pos_fname = ['Generated_' filepair{2}];  
        else
		  imagelist(i).pos_fname = filepair{2};
        end
	end

	% read next entry
	filepair = mvpr_lread(fh);
end; 

fprintf('\n');
mvpr_lclose(fh);

S = struct(...
    'numOfEvidences', numOfEvidences,...
    'numOfImgs', numOfImgs, ...
    'Xrange', [minX maxX], ...
    'Yrange', [minY maxY], ...
    'aspRatRange',[minAspRat maxAspRat], ...
    'evidDistRange',[minEvidDist maxEvidDist], ...
    'normEvidDistRange',[minNormEvidDist maxNormEvidDist], ...
    'evidDists', evidDists, ...
    'normEvidDists', normEvidDists, ...
    'passed', {imagelist}, ...
    'failed', {failedlist});


% -----------------------------------------------------------------------------

function props = try_image(imgname, posname, ptr)

% Try and check the training image and related data file.
% Return property structure if data is good,
% empty matrix otherwise.

props = [];

try
	evidPos = load(posname); % load evidence points
	info = imfinfo(imgname);
catch
  [errormsg errorid] = lasterr;
  switch (lower(errorid))
   case 'matlab:load:couldnotreadfile'
    warning(['Could not read file: ' imgname ' or ' posname]);
  end;
  return;
end

% check that all evidencepoints are given and calculate statistics
if (size(evidPos,1) == ptr.numOfEvidences & size(evidPos,2) == 2)
  evidDist = sqrt(sum((evidPos(1,:)-evidPos(end,:)).^2));
  normEvidDist = evidDist/sqrt(info.Width^2+info.Height^2);
else
	return;
end;

% We have a good training image.

% angle between left and right evidences points that should be
% used in the normalization
% NOTE: this is informational only, not really used.

props = struct(...
    'width', info.Width, ...
    'height', info.Height, ...
    'evidDist', evidDist,...
    'normEvidDist', normEvidDist);
