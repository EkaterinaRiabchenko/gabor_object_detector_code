%O3D2D_EVEX_HEAPUPDATE - Find maximum values from images
%
%  A min-heap (minumum value at the top) implementation for Matlab.
%  It can be used for finding largest values with their coordinates
%  from one or more images. 
%
%  evex_heapupdate(heap, image, shiftParams)
%  evex_heapupdate(heap,'sort')
%
%  'heap' is a struct, which must be initialized before calling the
%  function. It will be modified in place. If the 'sort' option
%  will be used, the heap will be sorted in place to descending
%  order (largest value at top). The heap structure will not
%  be a valid min-heap after sort operation.
%
%  The fields of 'heap' are:
%    maxn - maximum number of elements in the heap
%    n    - how many are in currently 
%    v    - two-dimensional heap presentation, create with e.g.
%           heap.v=zeros(elements,maxn)
%           (Elements depends on length of shiftParams, each element is
%            a vector of <value, coordinates (x,y), shiftParams>, which 
%            means that elements=5 with two shiftParams).
%
%  image  - two dimensional array of double values, from which the largest
%           will be placed in the heap with their locations.
%
%  The implementation might be a bit fragile. Invalid heap structure,
%  or inputs of wrong types, or if you look at the code wrong way, will
%  likely lead to segfaults or worse. Making nice error-checking seems
%  pretty pointless because mexErrMsgTxt(), which is the correct way of
%  displaying errors from MEX-functions, will crash Matlab anyway.
%
%  Compilation: mex heapupdate.c
%  The mex-command can be found in default matlab/bin directory, 
%  like 'matlab'.
%
% See also O3D2D_EVEX_HEAPINITIALIZE.M and
%          O3D2D_EVEX_EXTRACTIMAGEFEATURES.M .
%
% References:
%
%  [1] Kamarainen, J.-K., A revised simple Gabor feature space:
%  supervised local part detector, Image and Vision Computing,
%  2011.
% 
%  [2] Kamarainen, J.-K., Ilonen, J., Learning and Detection of
%  Object Landmarks in Canonical Object Space, 20th Int. Conf. on
%  Pattern Recognition (ICPR2010)  (Istanbul, Turkey, 2010).
%
% Author(s):
%    Jarmo Ilonen <Jarmo.Ilonen@lut.fi>
%
% Copyright:
%
%   Part detector (evidence extraction, *_evex_*) toolbox is
%   Copyright (C) 2008-2010 by Joni-Kristian Kamarainen and Jarmo
%   Ilonen.
%
