%O3D2D_EVEX_HEAPINITIALIZE - Initialize heap structure
%
%  heap=evex_heapinitialize(maxn)
%
% Heap-structure can be used with evex_heapupdate function.
% Currently, odd number of elements is required, so that the
% last leaf of the tree will not be alone (i.e. its parent
% will have two children). 
%
% See also O3D2D_EVEX_HEAPUPDATE.M and
%          O3D2D_EVEX_EXTRACTIMAGEFEATURES.M .
%
% References:
%
% Author(s):
%    Jarmo Ilonen <Jarmo.Ilonen@lut.fi>
%
% Copyright:
%
%   Part detector (evidence extraction, *_evex_*) toolbox is
%   Copyright (C) 2008-2010 by Joni-Kristian Kamarainen and Jarmo
%   Ilonen.
%
function heap=o3d2d_evex_heapinitialize(maxn)

%if mod(maxn,2)==0
%  error('maxn must be odd');
%  return;
%end;  

heap.n=0;
heap.maxn=maxn;
%heap.miss=0;
%heap.hit=1;

heap.v=zeros(5,maxn);
