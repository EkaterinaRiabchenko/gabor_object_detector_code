%EVEX_EXTRACTIMAGEFEATURES2 Extract object landmarks from image
%
%
% This function extracts multi-resolution Gabor features over the
% whole image and computes likelihood of each pixel with
% classifierS_. Scale and orientation searchs are applied. The
% function returns the best landmarks of each type (bestEvidences)
% and (only if requested) also the full likelihood maps of each
% landmark in mapData.
%
% NOTE: Prior to this function you may need to run
% EVEX_TRAINFEATURECLASSIFIER3.M .
%
% Output:
%  bestEvidences - {lm_type}[num_of_ev][1:5] Cell array of
%                  extracled landmarks (five valyes are
%                  x,y,confidence(likelihood),scale shift, rotation
%                  shift)
%  mapData       - Structure containing likelihood and quantile
%                  values for each landmark type (nice for
%                  debugging or MCMC spatial models)
%
% Input:
%  classifierS_- Classifier structure used for classifying
%  img_        - Gray level image to be processed (the type should
%                correspond to those used in training)
%  eigVect_     - The randomly chosen combination of gabor filters (their indexes in the bank)
%
%  <optional>
%  gabor_fmax     - Maximum (base) Gabor bank frequency [1,2]
%                   (typically in {1/20,1/30,1/40,1/50}, def. 1/20)
%  gabor_fnum     - Number of frequencies (typically in {3,4,6},
%                   def. 4) [1,2]
%  gabor_thetanum - Number of orientations (typically in {4,6,8},
%                   def. 6) [1,2]
%  gabor_k_       - The frequency scaling factor - starting from the
%                   base frequency (typically in
%                   {sqrt(2),sqrt(3),sqrt(4)}, def. sqrt(3)) [1,2]
%  gabor_p       - Gabor filter envelope overlap [0,1] (def. 0.65)
%  gabor_scaleShifts   - List of bank shifts applied to scale
%                   dimension (jump is of factor k_) [s_1 s_2 ...],
%                   e.g., [0 1], [-1 0 1] (Def. [0])
%  gabor_orientShifts  - List of bank shifts applied to orientation
%                   dimension [o_1 o_2 ...], e.g., [-1 0 1],
%                   (Def. [-1 0 1])
%  numOfBestLMs  - How many best returned (no more than this, can
%                   be less)
% 'debugLevel'    - 0,.. (Def. 0)
% 'normInput'          - Whether the input vectors are normalised to
%                        1: unit energy (DEFAULT) or
%                        0: not normalised at all
% 'numberTransform'    - Numerical transform of possible complex
%                        valued inputs:
%                         1: abs()
%                         2: [real() imag()]
%                         3: [abs() angle()]
%                         4: [abs() sin(angle()) cos(angle())];
%                         otherwise: None (i.e. complex numbers) (DEFAULT)
% 'gmmbFractHistSize'  - Used to init transform from likelihood to
%                        confidence (see gmmb_generatehist)
%                        (DEFAULT=10000)
% 'searchHeapSize'     - Used in evex_heapinitialize to init it for
%                        evex_heapupdate for fast update of best
%                        evidences (DEFAULT=2^16-1)
% 'gaborBank_p'        - Gabor filter envelope overlap [0,1]
%                        (DEFAULT: 0.65)
% 'maxDownScale'       - How many times the input image is allowed
%                        to be downscaled (decreases spatial
%                        accuracy, but affects to speed
%                        substantially) (DEFAULT=4);
%
% See also O3D2D_EVEX_TRAINFEATURECLASSIFIER3.M, GMMB_GENERATEHIST.M
%          and GMMB_CLASSIFY .
%
% References:
%
%  [1] Kamarainen, J.-K., A revised simple Gabor feature space:
%  supervised local part detector, Image and Vision Computing,
%  2011.
%
%  [2] Paalanen, P., Kamarainen, J.-K., Ilonen, J., Kälviäinen, H.,
%  Feature Representation and Discrimination Based on Gaussian
%  Mixture Model Probability Densities - Practices and Algorithms,
%  Pattern Recognition 39, 7 (2006) 1346-1358.
%
% Author(s):
%    Joni Kamarainen <Joni.Kamarainen@tut.fi>
%    Jarmo Ilonen <Jarmo.Ilonen@lut.fi>
%
% Copyright:
%
%   Part detector (evidence extraction, *_evex_*) toolbox is
%   Copyright (C) 2008-2010 by Joni-Kristian Kamarainen and Jarmo
%   Ilonen.
%
function [bestEvidences mapData] = o3d2d_evex_extractimagefeatures2(classifierS_, eigVect_, img_, varargin)

% 1. Parse input arguments
conf = struct(...
    'gabor_fmax', 1/20,...
    'gabor_fnum', 4,...
    'gabor_thetanum', 6,...
    'gabor_k', sqrt(3),...
    'gabor_p', 0.65,...
    'gabor_scaleShifts', 0,...
    'gabor_orientShifts', [-1 0 1],...
    'gabor_maxDownScale',4,...
    'numOfBestLMs', 100,...
    'debugLevel', 0,...
    'normInput', 1,...
    'numberTransform',0,...
    'gmmbFractHistSize', 10000,...
    'searchHeapSize', 2^16-1);
conf = mvpr_getargs(conf,varargin);

if (sum(conf.gabor_scaleShifts < 0)) % contains negative scale shifts
    sortedShifts = sort(conf.gabor_scaleShifts);
    effective_fmax = (conf.gabor_k)^-sortedShifts(1)*conf.gabor_fmax;
    effective_fmax_index = -sortedShifts(1)+1;
    effective_scaleShifts = sort(unique([sortedShifts-sortedShifts(1) 0:-sortedShifts(1)]));
else
    effective_fmax = conf.gabor_fmax;
    effective_fmax_index = 1;
    effective_scaleShifts = conf.gabor_scaleShifts;
end;


% 2. Prepare confidence estimation if PDF maps requested
% heapsize should be 2^n-1
heapSize=conf.searchHeapSize;
numOfParams = 3; % confidence val + extra params

sizeClassifierS = size(classifierS_{1},2);
if nargout > 1
    for i=1:sizeClassifierS
        fractHist(i).hist=gmmb_generatehist(classifierS_{1}(i),conf.gmmbFractHistSize);
    end;
    savePdfMaps = true;
end;

% cell array - entry for each evidence type - init values
evidences = cell(sizeClassifierS,1);

% intialize heaps for all evidence classes
for i=1:sizeClassifierS
    evidences{i}=o3d2d_evex_heapinitialize(heapSize);
end;
%%
% Extract Gabor features

% Create Gabor filter bank of given image size
warning('off','sg_createfilterbank:largeDimensionFactor');
testgaborBank = mvpr_sg_createfilterbank(...
    size(img_),...
    effective_fmax,...
    conf.gabor_fnum,...
    conf.gabor_thetanum,...
    'pf',0.9999,...
    'k',conf.gabor_k,...
    'p',conf.gabor_p,...
    'extra_freq',max(effective_scaleShifts));
warning('on','sg_createfilterbank:largeDimensionFactor');

G = mvpr_sg_filterwithbank(img_,testgaborBank,'method',1,'max_zoom',conf.gabor_maxDownScale);

% Convert to sample matrices
pG = mvpr_sg_resp2samplematrix(G)./prod(G.actualZoom);

% borderCrop (max. 20)
effEnv = testgaborBank.freq{1}.orient{1}.envelope;
borderCrop = min(20,max([effEnv(2)-effEnv(1) effEnv(4)-effEnv(3)]));
%realCrop=ceil(configS.borderCrop/G.zoom);
realCrop=ceil(borderCrop/G.zoom);
pG=pG(1+realCrop:end-realCrop,1+realCrop:end-realCrop,:);

% intialize data-structure for saving pdf-maps
if savePdfMaps
    mapData.actualZoom=G.actualZoom;
    mapData.realCrop=realCrop;
    count=1;
end;

%
% Go over different scales and orientations
for scaleShift = effective_scaleShifts
    %% scale matrix
    sGr = mvpr_sg_scalesamples(pG,scaleShift,conf.gabor_fnum,conf.gabor_thetanum);
    
    % normalize responses if necessary
    if (conf.normInput == 1)
        sGr = mvpr_sg_normalizesamplematrix(sGr);
    end;
    
    for orientShift = conf.gabor_orientShifts
        shiftParams = [scaleShift-effective_fmax_index orientShift];
        
        %%%%%%%%%%%%%%% DEBUG 2 %%%%%%%%%%%%%%%%%%
        if (conf.debugLevel >= 4)
            fprintf('[DB2] scaleShift:%2d, orientShift:%2d ', ...
                scaleShift, orientShift);
        end;
        %%%%%%%%%%%%%%% DEBUG 2 %%%%%%%%%%%%%%%%%%
        
        % rotate matrix
        spGr = mvpr_sg_rotatesamples(sGr,orientShift,conf.gabor_thetanum);
        
        if (conf.numberTransform==1)
            spGr=abs(spGr);
        elseif (conf.numberTransform==2)
            foo(:,:, 1 : size(spGr,3) )=real(spGr);
            foo(:,:, size(spGr,3)+1 : 2*size(spGr,3) )=imag(spGr);
            spGr=foo;
        elseif (conf.numberTransform==3)
            foo(:,:, 1 : size(spGr,3) )=abs(spGr);
            foo(:,:, size(spGr,3)+1 : 2*size(spGr,3) )=angle(spGr);
            spGr=foo;
        elseif (conf.numberTransform==4)
            foo(:,:, 1 : size(spGr,3) )=abs(spGr);
            foo(:,:, size(spGr,3)+1 : 2*size(spGr,3) )=sin(angle(spGr));
            foo(:,:, 2*size(spGr,3)+1 : 3*size(spGr,3) )=cos(angle(spGr));
            spGr=foo;
        end;
        
        %%%%%%%%%%%%%% DEBUG[2] %%%%%%%%%%%%%%%%%%%%%%%%%%%
        if (conf.debugLevel >= 5)
            for dbgInd1 = 1:size(spGr,3)
                imagesc(real(spGr(:,:,dbgInd1)));
                axis off;
                input(['Debug[2]: Real response for filter number ' num2str(dbgInd1) ...
                    ' <RETURN>']);
            end;
        end;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %  construction of a likeliood map
        clear T
        tmp1 = (reshape(spGr,[size(spGr,1)*size(spGr,2) size(spGr,3)]));
        eigVect = [];
        for q = 1:length(classifierS_)
            eigVect = tmp1(:,eigVect_(q,:));
            eigVect = eigVect./repmat(sqrt(sum(abs(eigVect).^2,2)),1,size(eigVect,2));
            T(:,:,q) = gmmb_pdf(eigVect, classifierS_{q});
        end
        if length(classifierS_)>1
        end
        T = prod(T,3);
        
        clear T_quantile
        if savePdfMaps
            for i=1:size(T,2)
                try
                    T_quantile(:,i)=gmmb_lhood2frac(fractHist(i).hist,T(:,i));
                catch
                    T_quantile(:,i) = zeros(size(T,1),1);
                    continue
                end
            end;
            T_quantile = reshape(T_quantile, [size(spGr,1) size(spGr,2) size(T_quantile,2)]);
        end;
        
        T = reshape(T, [size(spGr,1) size(spGr,2) size(T,2)]);
        
        % update better evidences (each evidence separately)
        for evInd = 1:length(evidences)
            o3d2d_evex_heapupdate(evidences{evInd}, T(:,:,evInd), shiftParams);
        end;
        clear evInd;
        
        if savePdfMaps
            if count==1
                tmpT =zeros(size(T));
            end
            tmpT = tmpT+T;% lhood maps for different shifts are summed up
            mapData.Tquantile{count}=T_quantile;
            mapData.scaleShift(count)=scaleShift;
            mapData.orientShift(count)=orientShift;
            mapData.gaborResp{count} = spGr;
            
            count=count+1;
        end;
        
    end; % orientShift
end; % scaleShift
mapData.T{1}=tmpT;

% fetch N best evidences

%%%%%%%%% DEBUG [2] %%%%%%%%%%%%%%%%%
if (conf.debugLevel >= 4)
    fprintf('[DB2] fetching N best');
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bestEvidences = cell(length(evidences),1);

% find the best evidence for each evidence-class
for evInd = 1:length(evidences)
    bestEvidences{evInd}=zeros(conf.numOfBestLMs,5);
    
    % sort the heap
    o3d2d_evex_heapupdate(evidences{evInd},'sort');
    % find best evidence while pruning those too close to border and too
    % near each other
    temp = o3d2d_evex_pruneevidence(evidences{evInd}.v,...
        conf.numOfBestLMs,...
        G.respSize,...
        0,... %configS.borderCrop/G.zoom,...
        0);%configS.evidenceMinDist/G.zoom);
    
    i=length(temp);
    % do the coordinate transforms back to original image coordinates
    % (border crop and zoom-level)
    bestEvidences{evInd}(1:i,1:2)=(G.zoom*(realCrop+evidences{evInd}.v(2:3,temp))+mean(G.actualZoom-1)/2)';
    % copy pdf values and shiftParams
    bestEvidences{evInd}(1:i,3)=evidences{evInd}.v(1,temp);
    cond(:,evInd) = [log10(max(evidences{evInd}.v(1,temp))) max(evidences{evInd}.v(1,temp))/mean(evidences{evInd}.v(1,temp))];
    bestEvidences{evInd}(1:i,4:5)=evidences{evInd}.v(4:5,temp)';
end;

clear evInd evPtsInd foo;

% %%%%%%%%% Debug[1] %%%%%%%%%%%%%%%%%%%%%%%%%%%
% if (conf.debugLevel >= 1)
%     for evInd = 1:length(bestEvidences)
%         imagesc(img_);
%         axis off;
%         colormap gray;
%         hold on;
%         for dbgInd2 = 1:min(10,size(bestEvidences{evInd},1))
%             text(bestEvidences{evInd}(dbgInd2,1),...
%                 bestEvidences{evInd}(dbgInd2,2),...
%                 num2str(dbgInd2),...
%                 'FontSize',18,'Color','black',...
%                 'BackgroundColor','white','EdgeColor','black');
%         end;
%         %figd = plot(bestEvidences{evInd}(:,1),bestEvidences{evInd}(:,2),'yo');
%         %set(figd,'MarkerSize',10);
%         %set(figd,'LineWidth',2);
%         hold off;
%         input(['Best extracted landmarks (max. 10) of ID=' num2str(evInd) ' <RETURN>']);
%     end;
% end;
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
