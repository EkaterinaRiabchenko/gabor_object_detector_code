%O3D2D_EVEX_EXTRACTIMAGEFEATURES_FILES3 Extract object landmarks from
%                                     image set
%
%
% This function load test images listed in testImagesListFile_ as
% <IMG_WITH_FULL_PATH> <GT_WITH_FULL_PATH>
% ...
% and then processes them using mvpr_evex_extractimagefeatures()
% function. Thise _files version is intended to use in experiments
% where a list of files are processed and extracted landmarks
% stored.
%
% NOTE: Prior to this function you may need to run
% O3D2D_EVEX_TRAINFEATURECLASSIFIER3.M .
%
% Output:
%  - (saved to files)
%
% Input:
%  [ALL THOSE IN O3D2D_EVEX_EXTRACTIMAGEFEATURES +]
%  classifierS - Classifier structure used for classifying, if
%                empty loaded from tempdir file
%  testImagesListFile_
%              - File name containing all test images (and
%                possibly also their ground truth)
%  cls_        - Name of the image category
%
%  <optional> [Same as with o3d2d_evex_extractimagefeatures.m]
%  numOfBestLMs  - How many best returned (no more than this, can
%                   be less)
% 'tempSaveDir'        - Output directory for all produced files
%                        (DEFAULT: '/tmp') - Note that you should
%                        specify this carefully as the system temp is
%                        cleaned periodically
% 'imageRoot'     - '.'
% 'classifierSaveFile' - File where the classifier struct saved
%                        (DEFAULT:
%                        'extracttrainfeatures_trainfeatures.mat')
% 'savePdfMaps'        - Whether full likehood maps are saved or
%                        not (ref. [1]) (DEFAULT=0)
% 'gpgImgPassphrase'   - If this is set, your images are first
%                        opened using this passphrase (usefull for
%                        confidential data) (DEFAULT=[])
%
% See also O3D2D_EVEX_EXTRACTIMAGEFEATURES.M and
%          O3D2D_EVEX_TRAINFEATURECLASSIFIER3.M .
%
% References:
%
%  [1] Kamarainen, J.-K., Ilonen, J., Learning, Detection and
%  Localisation of 2D Object Landmarks, CVPR2008, 2008.
%
%  [2] Paalanen, P., Kamarainen, J.-K., Ilonen, J., Kälviäinen, H.,
%  Feature Representation and Discrimination Based on Gaussian
%  Mixture Model Probability Densities - Practices and Algorithms,
%  Pattern Recognition 39, 7 (2006) 1346-1358.
%
%  [3] Riabchenko, E., Kamarainen, J.-K., Chen, Ke, Learning
%  Generative Models of Object Parts from A Few Positive Examples,
%  22nd Int. Conf. on Pattern Recognition (ICPR2014) (Stockholm, 2014)
%
% Author(s):
%    Joni Kamarainen <Joni.Kamarainen@tut.fi>
%    Jarmo Ilonen <Jarmo.Ilonen@lut.fi>
%    Ekaterina Riabchenko <Ekaterina.Riabchenko@lut.fi>
%
% Copyright:
%
%   Part detector (evidence extraction, *_evex_*) toolbox is
%   Copyright (C) 2008-2010 by Joni-Kristian Kamarainen and Jarmo
%   Ilonen.
%
function [bestEvidences] = o3d2d_evex_extractimagefeatures_files3(classifierS_, ...
    testImagesListFile_,cls_,varargin)

% 1. Parse input arguments
conf = struct(...
    'gabor_fmax', 1/20,...
    'gabor_fnum', 4,...
    'gabor_thetanum', 6,...
    'gabor_k', sqrt(3),...
    'gabor_p', 0.65,...
    'gabor_scaleShifts', 0,...
    'gabor_orientShifts', 0,...
    'gabor_maxDownScale',4,...
    'numOfBestLMs', 10,...
    'tempSaveDir', '/tmp',...
    'debugLevel', 0,...
    'imageRoot', '',...
    'landmarkRoot', '',... % for debug plots only
    'classifierSaveFile', 'trainfeatureclassifier_classifier.mat',...
    'normInput', 1,...
    'numberTransform',0,...
    'savePdfMaps',1,...
    'gmmbFractHistSize', 10000,...
    'searchHeapSize', 2^16-1,...
    'gpgImgPassphrase',[]);
conf = mvpr_getargs(conf,varargin);

%
% 2. Read classifier if not given
if (isempty(classifierS_))
    fprintf(['   Loading classifier  ''' ...
        fullfile(conf.tempSaveDir,conf.classifierSaveFile) ...
        '''...']);
    try
        load(fullfile(conf.tempSaveDir,conf.classifierSaveFile),...
            'classifierS');
    catch
        error('Classifier structure empty and cannot load one!');
    end;
    fprintf('done!\n');
else
    classifierS = classifierS_;
    clear classifierS_;
end;

try
    load(fullfile(conf.tempSaveDir,sprintf('indG_%s.mat',cls_)));
    eigVect = indG;
catch
    eigVect = 0;
end

%
% 3. Go through all test images and extract evidences and save them
%
% open file that contains image and position files
fh = mvpr_lopen(testImagesListFile_, 'read');

% read image and position file names
filepair = mvpr_lread(fh);

% heapsize should be 2^n-1
heapSize=conf.searchHeapSize;


imgInd = 0;
while ~isempty(filepair)
    imgInd = imgInd+1;
    loopInfoString = sprintf('Image#:%4d', imgInd);
    %   fprintf(loopInfoString);
    
    % read image and evidence locations
    imPath = fullfile(conf.imageRoot,filepair{1});
    
    if (~isempty(conf.gpgImgPassphrase))
        Tgimg = mvpr_imread(imPath, 'range', [0 1], 'type', 'double',...
            'gpgpassphrase',conf.gpgImgPassphrase);
    else
        Tgimg = mvpr_imread(imPath, 'range', [0 1], 'type', 'double');
    end;
    
    % Extract Gabor features
    
    % Save evidences
    loopInfoString = sprintf('%s-> saving',loopInfoString);
    fprintf(loopInfoString);
    % Write extracted evidences to a file in a ranked order
    [evDir foo evExt] = fileparts(filepair{1});
    [foo evFile foo] = fileparts(filepair{2});
    detSaveFile = [fullfile(conf.tempSaveDir,evDir,evFile) '.det'];
    % mkdir if a case it does not exist
    if (isdir(fileparts(detSaveFile)) == 0)
        mkdir(fileparts(detSaveFile));
    end;
    
    for cand = 1:length(classifierS)
        clsr{1} = classifierS{cand};
        [bestEvidences mapData{cand}] = ...
            o3d2d_evex_extractimagefeatures2(clsr, eigVect(cand,:), Tgimg, ...
            'gabor_fmax', conf.gabor_fmax,...
            'gabor_fnum', conf.gabor_fnum,...
            'gabor_thetanum', conf.gabor_thetanum,...
            'gabor_k', conf.gabor_k,...
            'gabor_p', conf.gabor_p,...
            'gabor_scaleShifts', conf.gabor_scaleShifts,...
            'gabor_orientShifts', conf.gabor_orientShifts,...
            'gabor_maxDownScale', conf.gabor_maxDownScale,...
            'numOfBestLMs', conf.numOfBestLMs,...
            'debugLevel', conf.debugLevel,...
            'normInput', conf.normInput,...
            'numberTransform', conf.numberTransform,...
            'gmmbFractHistSize', conf.gmmbFractHistSize,...
            'searchHeapSize', conf.searchHeapSize);
    end
    
    x = (mapData{1}.actualZoom(1))*((1:size(mapData{1}.T{1},1)) + mapData{1}.realCrop) + (mapData{1}.actualZoom(1)-1)/2;
    y = (mapData{1}.actualZoom(2))*((1:size(mapData{1}.T{1},2)) + mapData{1}.realCrop) + (mapData{1}.actualZoom(2)-1)/2;
    
    for i = 1:length(clsr{1})
        mx = 0;
        lh_mask = ones(size(mapData{1}.T{1}(:,:,1)));
        for j = 1:length(mapData)
            temp = mapData{j}.T{1};
            lh = temp(:,:,i);
            [b] = sort(lh(:),'descend');
            th_ind = round(numel(lh)*0.4);
            thld = b(th_ind);
            lh(lh<thld) = min(lh(:));
            lh_mask = lh.*lh_mask;
        end
        tmp_mask{i} = lh_mask;
        
        [b] = sort(lh_mask(:),'descend');
        thld3 = round(numel(lh_mask)*0.2);
        thld2 = b(thld3);
        
        if sum(lh_mask>thld2)==0 % if the object part is not found, give out location (1,1)
            lhood{i} = realmin;
            xx{i} = 1;
            yy{i} = 1;
        else
            cnt = 1;
            bg_lh = min(lh_mask(find(lh_mask>0)));
            lh_mask(lh_mask<= thld2) = 0;
            while (sum(lh_mask(:))~=0 && cnt<=conf.numOfBestLMs )
                mx(cnt) = max(lh_mask(:));
                [row(cnt),col(cnt)] = find(lh_mask==max(lh_mask(:)),1,'first');
                area = ceil(20/mean(mapData{1}.actualZoom));
                lh_mask(max(row(cnt)-area,1):min(row(cnt)+area,size(lh_mask,1)),...
                    max(col(cnt)-area,1):min(col(cnt)+area,size(lh_mask,2))) = 0;
                cnt = cnt + 1;
            end
            
            lhood{i} = mx;
            xx{i} = (mapData{1}.actualZoom(1))*(col + mapData{1}.realCrop) - (mapData{1}.actualZoom(1)-1)/2;
            yy{i} = (mapData{1}.actualZoom(2))*(row + mapData{1}.realCrop) - (mapData{1}.actualZoom(2)-1)/2;
            
        end
        if conf.debugLevel == 2
            % visualize candidate locations ofr each object part separately
            f = figure('Visible','off');
            imshow(Tgimg)
            hold on
            plot(xx{i},yy{i},'r*')
            ind1 = strfind(conf.tempSaveDir,'/');
            ind2 = strfind(evDir,'/');
            imSaveFile = fullfile(conf.tempSaveDir,'Detections',evDir(ind2(end)+1:end),[evFile '_' num2str(i) '_resTest.png']);
            [imDir,foo,foo] = fileparts(imSaveFile);
            if ~isdir(imDir)
                mkdir(imDir)
            end
            saveas(gcf,imSaveFile,'png');
            clear tmp zimg row col
            close(f);
        end
    end
    
    if conf.debugLevel == 2
        % visualisation of the canditate detections (yellow dots) with
        % corresponding lhood maps (10% highest values) on top
        
        %         f = figure('Visible','off');
        %         imshow(Tgimg);
        %         axis off;
        %         colormap gray;
        %         hold on;
        for evInd = 1:length(bestEvidences)
            
            %                 for dbgInd2 = 1:min(1,size(bestEvidences{evInd},1))
            %                     text(xx{evInd}(dbgInd2),...
            %                         yy{evInd}(dbgInd2),...
            %                         num2str(evInd),...
            %                         'FontSize',15,'Color','blue');
            %                     plot(xx{evInd}(dbgInd2),yy{evInd}(dbgInd2),'r.','MarkerSize', 13)
            %                 end;
            
            dbgImgStep = 2;
            dbgFractile = 0.8; % actually inverse [1,0]->[0,1]
            T = tmp_mask{evInd};
            fooPdf = log(T(1:dbgImgStep:end,1:dbgImgStep:end));
            fooSortPdf = sort(fooPdf(:));
            length(fooSortPdf)
            if (dbgFractile == 0)
                fooFracVal  = -inf;
            else
                round(dbgFractile*length(fooSortPdf))
                fooFracVal = fooSortPdf(round(dbgFractile*length(fooSortPdf)));
            end;
            fooFracImg = zeros(size(fooPdf));
            fooFracImg(:) = NaN;
            fooFracImgInds = find(fooPdf >= fooFracVal);
            fooFracImg(fooFracImgInds) = fooPdf(fooFracImgInds);
            
            crop = round(mean([mapData{1}.actualZoom(1)*mapData{1}.realCrop;...
                mapData{1}.actualZoom(2)*mapData{1}.realCrop])...
                -(mapData{1}.actualZoom(1)-1)/2);
            Tgimg2 = Tgimg(crop+1:end-crop,crop+1:end-crop);
            x = xx{evInd}-crop+(mapData{1}.actualZoom(1)-1)/2;
            y = yy{evInd}-crop+(mapData{1}.actualZoom(2)-1)/2;
            f = figure('Visible','off');
            mvpr_zimage(Tgimg2(1:dbgImgStep:end,1:dbgImgStep:end),fooFracImg,x/dbgImgStep,y/dbgImgStep,'figure','false','alpha',0.5);
            hold on
            axis off
        end
        ind1 = strfind(conf.tempSaveDir,'/');
        ind2 = strfind(evDir,'/');
        imSaveFile = fullfile(conf.tempSaveDir,'Detections',conf.tempSaveDir(ind1(end)+1:end),[evFile '_lhmap_tets.png']);
        [imDir,foo,foo] = fileparts(imSaveFile);
        if ~isdir(imDir)
            mkdir(imDir)
        end
        saveas(gcf,imSaveFile,'png');
        clear tmp zimg
        close all;
    end
    
    % save candidate locations into a file
    detFh = mvpr_lopen(detSaveFile,'write');
    for evInd = 1:length(lhood)
        for detInd = 1:length(lhood{evInd})
            fprintf(detFh.fid,'%d %f %f %e %f %f\n',...
                evInd,...  % label
                xx{evInd}(detInd),...  % x
                yy{evInd}(detInd),...  % y
                lhood{evInd}(detInd),...  % confidence
                bestEvidences{evInd}(detInd,4),...  % scale shift #
                bestEvidences{evInd}(detInd,5));    % rot shift #
        end;
    end;
    mvpr_lclose(detFh);
    
    % save pdf-map structure
    if conf.savePdfMaps
        mapSaveFile = [fullfile(conf.tempSaveDir,evDir,evFile) '.mat'];
        save(mapSaveFile,'mapData');
    end;
    
    % read image and position file names
    filepair = mvpr_lread(fh);
    % flush info line
    fprintf(['\r',repmat(' ',1,length(loopInfoString))]);
end;
fprintf(' Done!\n');
clear xx yy mx lhood

mvpr_lclose(fh);

fprintf(['   Evidences extracted from the test images and saved into ' ...
    'temporary working directory (*.det for points and *.mat for ' ...
    'likelihood maps)']);



