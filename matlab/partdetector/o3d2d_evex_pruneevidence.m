%O3D2D_EVEX_PRUNEEVIDENCE - Prunes evidence close to border/each
%                           other 
%
% Returns best evidence which are not too close to image border
% or to a better evidence. Heap (from o3d2d_evex_heapupdate) can be  
% used as input after sorting.
%
% ind = evex_pruneevidence(ev,n,dim,borderCrop,evidenceMinDist)
%
% 'ev'              - a two-dimensional array where ev(1,:) are 
%                     the values and ev(2:3,:) are the coordinates
%                     (heap can be used directly as input)
% 'n'               - maximum number of evidences to return
% 'dim'             - the size of the are, [x y]
% 'borderCrop'      - minimum distance from the evidence to the border
% 'evidenceMinDist' - minumum distance between evidences.
%
% See also O3D2D_EVEX_HEAPUPDATE.M and
%          O3D2D_EVEX_EXTRACTIMAGEFEATURES.M .
%
% References:
%
%  [1] Kamarainen, J.-K., A revised simple Gabor feature space:
%  supervised local part detector, Image and Vision Computing,
%  2011.
% 
%  [2] Kamarainen, J.-K., Ilonen, J., Learning and Detection of
%  Object Landmarks in Canonical Object Space, 20th Int. Conf. on
%  Pattern Recognition (ICPR2010)  (Istanbul, Turkey, 2010).
%
% Author(s):
%    Jarmo Ilonen <Jarmo.Ilonen@lut.fi>
%
% Copyright:
%
%   Part detector (evidence extraction, *_evex_*) toolbox is
%   Copyright (C) 2008-2010 by Joni-Kristian Kamarainen and Jarmo
%   Ilonen.
%
function ind=o3d2d_evex_pruneevidence(ev,n,dim,borderCrop,evidenceMinDist)

% extra space to bitmap if evidenceMinDist is larger than borderCrop
extra=ceil(evidenceMinDist-borderCrop);
if extra<0
  extra=0;
end;

% initialize bitmap if it is needed
if evidenceMinDist>0
  bitmap=zeros(dim(2)+extra*2,dim(1)+extra*2);
end;    

% the stupid indices start at 1
extra=extra+1;



loc=ev(2:3,:);

% crop evidence too close to border
if borderCrop>0
  dim2=repmat(dim',1,size(ev,2));
  evInd=find(min([loc ; dim2-loc])>borderCrop);
else
  evInd=1:size(ev,2);
end;

ind=[];
accepted=0;

if evidenceMinDist>0

  % remove evidence too close to other better evidence

  for i=evInd,
    
    if bitmap(round(loc(2,i)+extra),round(loc(1,i)+extra))<1
      bitmap(round((loc(2,i)-evidenceMinDist:loc(2,i)+evidenceMinDist)+extra),...
             round((loc(1,i)-evidenceMinDist:loc(1,i)+evidenceMinDist)+extra) )=1;
    else
      continue;
    end;
    
    % the evidence was accepted, whee
    accepted=accepted+1;
    ind(accepted)=i;
    if accepted>=n
      break;
    end;
  end;
  
else
  
  if n>length(evInd)
    ind=evInd;
  else
    ind=evInd(1:n);
  end;        
end;

if 0
  imagesc(bitmap);
  hold on
  plot(ev(2,:)+extra,ev(3,:)+extra,'r.')
  plot(ev(2,ind)+extra,ev(3,ind)+extra,'ko');
  hold off
end;

%save /tmp/prune.mat
