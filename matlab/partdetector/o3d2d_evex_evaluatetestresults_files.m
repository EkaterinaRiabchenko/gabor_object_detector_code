%O3D2D_EVEX_EVALUATETESTRESULTS_FILES Evaluate extracted landmarks
%                                     against ground truth
%
%
% This function loads the extracted landmarks for the test image in
% testImagesListFile_ and their ground truth and creates graphical
% representations of the accuracy.
%
% NOTE: Prior to this function you may need to run
% EVEX_EXTRACTIMAGEFEATURES.M .
%
% Output:
%  evalP - Evaluation structure containing those evaluation results
%          plotted in the interactive mode.
%
% Input:
%  testImagesListFile_
%                 - File name containing all test images (and
%                   possibly also their ground truth)
%  genLmFlag_     - Used to chnge paths, when automatically generated
%                   landmarks are used
%  cls_           -  Name of the image category
%
%  <optional>
%  'numOfLMs'     - Number of landmarks to be expected to be found
%                   (guessed from the first ground truth file)
%  'numOfBestLMs' - Only this many per landmark included to
%                   evaluation (Def. 10) (cannot be more than
%                   extracted)
% 'tempSaveDir' - Output directory for all produced files (DEFAULT:
%                 '/tmp') - Note that you should specify this
%                 carefully as the system temp is cleaned
%                 periodically
% 'debugLevel'  - {0<DEFAULT>,1,2}
% 'testDists'   - Normalised distances under which the statistics
%                 are created (supports upto 3) (DEFAULT: [0.05 0.1
%                 0.25])
% 'deyeLMs'     - Which GT landmarks used to make the normalised
%                 distance (DEFAULT [<first> <last>]
% 'extrLMs'     - Which extracted landmarks are considered in the
%                 statistics (DEFAULT [<first>:<last>]
% 'gtLMs'       - Which GT landmarks correspond to those of extrLMs
%                 (DEFAULT [<first>:<last>], e.g. one-2-one
%                 correspondence
% 'plotSaveFile'- Plot save file - if given, nothing printed, but
%                 saved to given file name (DEFAULT: [])
% 'noPlot'      - No result plots (Def. 0)
%
% See also O3D2D_EVEX_EXTRACTIMAGEFEATURES.M .
%
% References:
%
%  [1] Kamarainen, J.-K., A revised simple Gabor feature space:
%  supervised local part detector, Image and Vision Computing,
%  2011.
%
%  [2] Kamarainen, J.-K., Ilonen, J., Learning and Detection of
%  Object Landmarks in Canonical Object Space, 20th Int. Conf. on
%  Pattern Recognition (ICPR2010)  (Istanbul, Turkey, 2010).
%
% Author(s):
%    Joni Kamarainen <Joni.Kamarainen@lut.fi>
%    Jarmo Ilonen <Jarmo.Ilonen@lut.fi>
%
% Copyright:
%
%   Part detector (evidence extraction, *_evex_*) toolbox is
%   Copyright (C) 2008-2010 by Joni-Kristian Kamarainen and Jarmo
%   Ilonen.
%
function [evalP] = o3d2d_evex_evaluatetestresults_files(genLmFlag_,cls_,testImagesListFile_, ...
    varargin);

% 1. Parse input arguments (FIRST ROUND - see the second as well)
conf = struct(...
    'numOfBestLMs', 10,...
    'numOfLMs', nan,...
    'tempSaveDir', '/tmp',...
    'debugLevel', 0,...
    'testDists', [0.05 0.1 0.25],...
    'gtLMs', [1:nan],... % all
    'extrLMs', [1:nan],... % one-2-one correspondence
    'plotSaveFile',[],...
    'imageRoot', '.',...
    'landmarkRoot', '.',...
    'noPlot', false);
conf = mvpr_getargs(conf,varargin);

% Load first ground truth file to acquire how many landmarks
fprintf(['   Guessing number of landmarks by reading the first ground ' ...
    ' truth (landmark) file...']);
fh = mvpr_lopen(testImagesListFile_, 'read');
filepair = mvpr_lread(fh);
if genLmFlag_
    [lmDir lmName ext] = fileparts(filepair{2});
    lmSaveFile = fullfile(conf.landmarkRoot,['Generated_' lmDir],cls_,[lmName ext]);
    evidPos = load(lmSaveFile);
else
    evidPos = load(fullfile(conf.landmarkRoot,filepair{2}));
end
guessedNumOfLandmarks = size(evidPos,1);
mvpr_lclose(fh);
fprintf('guessed %d (used in normalisation)... Done!\n',guessedNumOfLandmarks);


% 1. Parse input arguments (SECOND ROUND - see the first as well)
conf = struct(...
    'numOfBestLMs', 10,...
    'numOfLMs', guessedNumOfLandmarks,...
    'tempSaveDir', '/tmp',...
    'debugLevel', 0,...
    'testDists', [0.05 0.1 0.25],...
    'gtLMs', [1:guessedNumOfLandmarks],... % all
    'extrLMs', [1:guessedNumOfLandmarks],... % one-2-one correspondence
    'plotSaveFile',[],...
    'imageRoot', '.',...
    'landmarkRoot', '.',...
    'noPlot', false);
conf = mvpr_getargs(conf,varargin);

%
% 1. Go through all test images, load extracted landmarks and
% groundtruth

% open file that contains image and position files
fh = mvpr_lopen(testImagesListFile_, 'read');

% read image and position file names
filepair = mvpr_lread(fh);

imgInd = 0;
totFoundCumHist = zeros(length(conf.testDists),conf.numOfBestLMs);
foundEvidences = [];
foundEvidenceCounts = [];
numEvidenceFound=zeros(length(conf.testDists),conf.numOfLMs+1); % hardcoded testdistances * evidences
randExmplFound = 0;
while ~isempty(filepair)
    imgInd = imgInd+1;
    loopInfoString = sprintf('\rImage#:%4d', imgInd);
    fprintf(loopInfoString);
    
    if genLmFlag_
        [lmDir lmName ext] = fileparts(filepair{2});
        lmSaveFile = fullfile(conf.landmarkRoot,['Generated_' lmDir],cls_,[lmName ext]);
        bbSaveFile = fullfile(conf.landmarkRoot,['Generated_' lmDir],[lmName '.box']);
    else
        [lmDir lmName ext] = fileparts(filepair{2});
        lmSaveFile = fullfile(conf.landmarkRoot,filepair{2});
        bbSaveFile = fullfile(conf.landmarkRoot,lmDir,[lmName '.box']);
    end
    
    %%
    
    %%%%%%%%%% DEBUG[1] %%%%%%%%%%%%%%%%%%%%%%
    if (conf.debugLevel >= 1)
        % load random example and demonstrate used error levels
        if (rand() < 0.1 & randExmplFound == 0)
            if (exist('gmmbvl_ellipse','file'))
                randImg = imread(fullfile(conf.imageRoot,filepair{1}));
                [dbg_lmDir dbg_lmName dbg_ext] = fileparts(filepair{2});
                dbg_lmSaveFile = fullfile(conf.landmarkRoot,filepair{2});
                dbg_bbSaveFile = fullfile(conf.landmarkRoot,dbg_lmDir,[dbg_lmName '.box']);
                dbg_LMs = load(dbg_lmSaveFile);
                dbg_bb = load(dbg_bbSaveFile);
                imagesc(randImg);
                axis off;
                hold on;
                fooColors = {'red','green','blue'};
                fooLegend = {};
                for fooInd = 1:min(3,length(conf.testDists))
                    fooRad = conf.testDists(fooInd)*...
                        sqrt(sum((bb(1,:)-bb(3,:)).^2,2));
                    fooh = gmmbvl_ellipse(fooRad,fooRad,0,...
                        dbg_LMs(conf.deyeLMs(1),1),...
                        dbg_LMs(conf.deyeLMs(1),2));
                    set(fooh,'LineWidth',2,'Color',fooColors{fooInd});
                    fooLegend = {fooLegend{1:end}, sprintf('%1.2f',conf.testDists(fooInd))};
                end;
                text(dbg_LMs(conf.deyeLMs(2),1),...
                    dbg_LMs(conf.deyeLMs(2),2),...
                    num2str(conf.deyeLMs(2)),...
                    'FontSize',18,'Color','black',...
                    'BackgroundColor','white','EdgeColor','black');
                legend(fooLegend);
                hold off;
                randExmplFound = 1;
                input('[DEBUG(1)] Illustration of detection distances <RETURN>');
            else
                randExmplFound = 1;
                warning(['[DEBUG(1)] Cannot demostrate error levels since ellipse ' ...
                    'plotting not available (GMMBAYES toolbox)'])
            end;
        end;
    end;
    %%%%%%%%%%%%%%%%%%%%%%%%%c%%%%%%%%%%%%%%%%%
    
    % read evidence locations
    evidPos = load(lmSaveFile);
    bb = load(bbSaveFile);
    
    if (size(evidPos,1) == conf.numOfLMs)
        % read extracted features
        [dotDir foo dotExt] = fileparts(filepair{1});
        [foo dotFile foo] = fileparts(filepair{2});
        detEvs = load(fullfile(conf.tempSaveDir,dotDir,[dotFile '.det']));
        
        evFoundCumHist = zeros(length(conf.testDists),conf.numOfBestLMs);
        eyeDist = sqrt(sum((bb(1,:)-bb(3,:)).^2,2));
        whichEvidenceFound = zeros(length(conf.testDists), length(conf.extrLMs));
        evidenceFoundCount = 0*ones(length(conf.testDists), length(conf.extrLMs));
        for evInd = 1:length(conf.extrLMs) % search one evidence number at time
            dists = detEvs(find(detEvs(:,1) == conf.extrLMs(evInd)), 2:3)-...
                repmat(evidPos(conf.gtLMs(evInd),:),length(find(detEvs(:,1) == conf.extrLMs(evInd))), 1);
            dists = sqrt(sum(dists.^2,2))./eyeDist;
            %       dists = dists(1:10,:);
            for testDistInd = 1:length(conf.testDists) % separately for each threshold
                foundInd = find(dists <= conf.testDists(testDistInd));
                if (~isempty(foundInd))
                    whichEvidenceFound(testDistInd, evInd) = 1;
                    evidenceFoundCount(testDistInd,evInd) = length(foundInd);
                    % following does not allow substitutions over max length
                    evFoundCumHist(testDistInd,foundInd(1):end) = evFoundCumHist(testDistInd,foundInd(1):end)+1;
                end;
            end;
        end;
        foundEvidences = [foundEvidences; shiftdim(whichEvidenceFound,-1)];
        foundEvidenceCounts = [foundEvidenceCounts; shiftdim(evidenceFoundCount,-1)];
        
        totFoundCumHist = totFoundCumHist+evFoundCumHist;
        
        % nfound = how many evidences found correctly for each distance limit
        nfound=sum(whichEvidenceFound,2);
        for i=1:length(conf.testDists)
            numEvidenceFound(i,nfound(i)+1)=numEvidenceFound(i,nfound(i)+1)+1;
        end;
    else
        warning(['Number of evidences do not match in ' fullfile(conf.landmarkRoot,filepair{2})]);
    end;
    % read image and position file names
    filepair = mvpr_lread(fh);
end;
mvpr_lclose(fh);

% Compute evaluation values for plotting and storing
totFoundCumHist = 100*totFoundCumHist./imgInd./length(conf.extrLMs);

tpr = totFoundCumHist/100;
totExtracted = (1:size(totFoundCumHist,2))*conf.numOfLMs;
fpr = repmat(totExtracted,[size(totFoundCumHist,1) 1])...
    -totFoundCumHist/100*conf.numOfLMs;
fpr = fpr./repmat(totExtracted,[size(totFoundCumHist,1) 1]);

[mx_val max_ind] = max(totFoundCumHist(1,:));

if (conf.noPlot == false)
    figure
    % Plot results
    if (~isempty(conf.plotSaveFile))
        subplot(1,2,1);
    else
        clf;
    end;
    plot(1:length(totFoundCumHist),totFoundCumHist(1,:),'k-',...
        1:length(totFoundCumHist),totFoundCumHist(2,:),'b--',...
        1:length(totFoundCumHist),totFoundCumHist(3,:),'r-.','LineWidth',2);
    figd = legend(['d = ' sprintf('%1.2f',conf.testDists(1))],...
        [ 'd = ' sprintf('%1.2f',conf.testDists(2))],...
        [ 'd = ' sprintf('%1.2f',conf.testDists(3))],...
        'Location','SouthEast');
    xlabel('number of each ext. landmarks','FontSize',18);
    ylabel('proportion of correct [%]','FontSize',18);
    axis([0 conf.numOfBestLMs 0 100])
    set(gca,'FontSize',18)
    grid(gca,'minor')
    if (isempty(conf.plotSaveFile))
        input(['\nCumulative detection histogram for the evidences used in the ' ...
            'distance normalisation (d_eye) <RETURN>']);
    end;
    
    numEvidenceFound=numEvidenceFound./imgInd;
    if (~isempty(conf.plotSaveFile))
        subplot(1,2,2);
    else
        clf;
    end;
    barh(0:(size(numEvidenceFound,2)-1),numEvidenceFound');
    figd = legend(['d = ' sprintf('%1.2f',conf.testDists(1))],...
        ['d = ' sprintf('%1.2f',conf.testDists(2))],...
        ['d = ' sprintf('%1.2f',conf.testDists(3))],...
        'Location','SouthEast');
    ylabel('# of found landmarks','FontSize',18)
    xlabel('proportion of images','FontSize',18)
    title(['For ' num2str(conf.numOfBestLMs) ' extracted landmark candidates'],'FontSize',15);
    axis([0 1 -0.5 size(numEvidenceFound,2)-0.5]);
    set(gca,'FontSize',18)
    grid on;
    if (isempty(conf.plotSaveFile))
        disp(['How many correct evidences found per image'...
            ' after extracting ' num2str(conf.numOfBestLMs)...
            ' per each type']);
    else
        saveas(gcf,conf.plotSaveFile,'png');
        close(gcf);
    end;
end;
% Evaluation structure to be returned
evalP.numOfBestLMs = conf.numOfBestLMs;
evalP.testDists = conf.testDists;
evalP.gtLMs = conf.gtLMs;
evalP.extrLMs = conf.extrLMs;
evalP.totFoundCumHist = totFoundCumHist;
evalP.foundEvidenceCounts = foundEvidenceCounts;
evalP.numEvidenceFound = numEvidenceFound;
evalP.tpr = tpr;
evalP.fpr = fpr;
evalP.mxInd = max_ind;

