%O3D2D_EVEX_MEANMODEL_FILES - Reads point patterns from files and
%                             forms a mean model
%
% [meanModel meanModelErr mmLMarks] = ...
%     o3d2d_evex_meanmodel_files(file_,bboxFlag_,genLmsFlag_,cls_,
%     homography_,:)
%
% This function requirest a list of images and their landmark
% files. It constructs the canonical object space [1,2] which is
% called as a "point pattern mean model".
%
% The function basically only wraps MVPR_H2D_MEANMODEL for the data
% files used in Object3Dto2D project and provides some nice debug
% plots.
%
% Output:
%  meanModel    - Mean model points (Xm in mvpr_h2d_meanmodel)
%  meanModelErr - Avg mapping error for the model (mgd in mvpr_h2d_meanmodel)
%  mmLMarks     - Lanmarks in the mean model space (Xt in mvpr_h2d_meanmodel)
%
% Input:
%  file_          - File with full path containing training images
%                   and landmark files (if empty, then
%                   'reportFile' read from the 'tempSaveDir')
%  homography_    - {'none','isometry','similarity','affinity','projectivity'}
%  bboxFlag_      - defines the use of bounding box corners in the mean
%                   model (true/false)
%  genLmsFlag_    - used to chnge paths, when automatically generated
%                   landmarks are used
%  cls_           - name of a category of images
%
%  <OPTIONAL>
% 'landmarkRoot'  - Root directory which should be used in front of
%                   every loaded file in trainListFile_ (Def. '.')
% 'debugLevel'    - 0,.. (Def. 0)

%
% See also  O3D2D_EVEX_EXTRACTTRAINFEATURES_FILES.M and MVPR_H2D_MEANMODEL.M .
%
% References:
%
%  [1] Kamarainen, J.-K., A revised simple Gabor feature space:
%  supervised local part detector, Image and Vision Computing,
%  2011.
% 
%  [2] Kamarainen, J.-K., Ilonen, J., Learning and Detection of
%  Object Landmarks in Canonical Object Space, 20th Int. Conf. on
%  Pattern Recognition (ICPR2010)  (Istanbul, Turkey, 2010).
%
% Author(s):
%    Joni Kamarainen <Joni.Kamarainen@tut.fi>
%    Ekaterina Riabchenko <Ekaterina.Riabchenko@lut.fi>
%
% Copyright:
%
%   Part detector (evidence extraction, *_evex_*) toolbox is
%   Copyright (C) 2008-2010 by Joni-Kristian Kamarainen and Jarmo
%   Ilonen.
%
function [meanModel meanModelErr mmLMarks] = ...
    o3d2d_evex_meanmodel_files(file_,bboxFlag_,genLmsFlag_,cls_, homography_, varargin);

% 1. Parse input arguments
conf = struct(...
    'debugLevel', 0,...
    'landmarkRoot', '.');
conf = mvpr_getargs(conf,varargin);

fh = mvpr_lopen(file_, 'read');

% read image and position file names
filepair = mvpr_lread(fh);

imgInd = 0;
lMarks = [];
while ~isempty(filepair)
	imgInd = imgInd+1;
        fprintf('\r      Reading landmarks: %5d...',imgInd);
        
        % read and store evidence locations
        [lmDir lmName ext] = fileparts(filepair{2});
        if bboxFlag_
            if ~genLmsFlag_
                lMarks = [lMarks;...
                         shiftdim(load(fullfile(conf.landmarkRoot,filepair{2})),-1)...
                         shiftdim(load(fullfile(conf.landmarkRoot, lmDir,[lmName '.box'])),-1)];
            else
                %just lmDir as we read a report file where it is already
                %added Generated_
                lMarks = [lMarks;...
                         shiftdim(load(fullfile(conf.landmarkRoot,lmDir,cls_,[lmName ext])),-1)...
                         shiftdim(load(fullfile(conf.landmarkRoot, lmDir,[lmName '.box'])),-1)];
            end             
        else 
         lMarks = [lMarks;...
             shiftdim(load(fullfile(conf.landmarkRoot,filepair{2})),-1)];   
        end
        
	% read next image and position file names
	filepair = mvpr_lread(fh);
end;
fprintf('Done!\n');

lmarks = [];
% check all possible pairs of landmarks; don't use if they have the same
% coordinates
for k = 1:length(lMarks)
    comb = nchoosek(1:size(lMarks,2),2);
    tmp = sum((lMarks(k,comb(:,1),:) - lMarks(k,comb(:,2),:)).^2,3);
    if sum(tmp==0)==0
     lmarks = [lmarks; lMarks(k,:,:)];
    end
end

if isempty(lmarks)
    lmarks = lMarks;
end


fprintf('      mean model computation...');
switch (homography_)
 case 'none'
  [meanModel meanModelErr mmLMarks] = mvpr_h2d_meanmodel(lmarks,0);
 case 'isometry'
  [meanModel meanModelErr mmLMarks] = mvpr_h2d_meanmodel(lmarks,1);
 case 'similarity'
  [meanModel meanModelErr mmLMarks] = mvpr_h2d_meanmodel(lmarks,2);
 case 'affinity'
  [meanModel meanModelErr mmLMarks] = mvpr_h2d_meanmodel(lmarks,3);
 case 'projectivity'
  [meanModel meanModelErr mmLMarks] = mvpr_h2d_meanmodel(lmarks,4);
 otherwise
  error('Unknown homography parameter!!');
end;

% gathering object scale and orientation statistics
alpha = [];
for i = 1:length(lmarks)
    H(:,:,i) = mvpr_h2d_corresp(shiftdim(lmarks(i,:,:),1)',meanModel','hType',homography_);
    A = H(1:2,1:2,i);
    [U S V] = svd(A);
    s(i) = S(1,1);
    t(:,i) = H(1:2,3);
    R = U*V';
    detR(i) = det(R);
    if abs(det(R)-1)<10^(-5)%det(R)==1
        if R(1,1)>0
            alpha = [alpha asind(-R(1,2))];
        else
            alpha = [alpha 2*90*sign(asind(-R(1,2))) - asind(-R(1,2))];
        end
    else
        warning('det not equal to 1');
    end
end

transformS.t_distrib = gmmb_fj(t','Cmax',1);
transformS.s_distrib = gmmb_fj(s','Cmax',1);
transformS.r_distrib = gmmb_fj(alpha','Cmax',1);

transformS.r_min = min(alpha);
transformS.r_max = max(alpha);
transformS.r_mean = mean(alpha);
transformS.r = alpha;

transformS.s_min = min(s);
transformS.s_max = max(s);
transformS.s_mean = mean(s);
transformS.s = s;

[saveDir foo foo]=fileparts(file_);
save(fullfile(saveDir,sprintf('transformStat_%s.mat',cls_)),'transformS');
%%%%%%%%%%%%%%%%%% DEBUG[1] %%%%%%%%%%%%%%%%%%%
if (conf.debugLevel >= 1)
  plot(meanModel(:,1),meanModel(:,2),'bd','LineWidth',2,'MarkerSize',15);
  clf;cla;
  %axis([0.9*min(meanModel(:,1)) 1.1*max(meanModel(:,1))...
  %      0.9*min(meanModel(:,2)) 1.1*max(meanModel(:,2))]);
  set(get(gcf,'CurrentAxes'),'YDir','reverse');
  axis off
  hold on;
  plot(mmLMarks(:,:,1),... % These help to plot axes properly (yes, a kludge)
       mmLMarks(:,:,2),'w.');
  for dbgInd = 1:size(mmLMarks,1)
    for dbgInd2 = 1:size(mmLMarks,2)
      text(mmLMarks(dbgInd,dbgInd2,1),...
           mmLMarks(dbgInd,dbgInd2,2),...
           num2str(dbgInd2),...
           'FontSize',14,'Color','black',...
           'BackgroundColor','white','EdgeColor','black');
    end;
  end;
  for dbgInd2 = 1:size(mmLMarks,2)
    text(meanModel(dbgInd2,1),...
         meanModel(dbgInd2,2),...
         num2str(dbgInd2),...
         'FontSize',18,'Color','black',...
         'BackgroundColor','yellow','EdgeColor','black');
  end;
  hold off;
  input(['Debug[1]: Mean model (yellow) and all examples transformed ' ...
         'to mean model space <RETURN>']);
end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mvpr_lclose(fh);
