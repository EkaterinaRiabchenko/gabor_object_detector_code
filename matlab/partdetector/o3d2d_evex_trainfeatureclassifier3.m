%O3D2D_EVEX_TRAINFEATURECLASSIFIER3 - Train a classifier for classifying
%                                    landmark features in the
%                                    standard pose
%
% [classifierS] = evex_trainfeatureclassifier(Ptrain_,Ttrain_,varargin)
%
% This function uses input features Ptrain_ and their corresponding
% landmark labels Ttrain_ to to train a "classifier" for
% classifying new inputs (NxM size Gabor response matrices). The
% classifier is actually a Gaussian mixture model trained by the
% unsupervised Figuiredo-Jain algorithm. Read ref. [2] for
% details.
%
% If you have enough training points the default values should work
% very well.
%
% NOTE: Prior to this function you may need to run
% EVEX_EXTRACTTRAINFEATURES.M .
%
% Output:
%  classifierS - Classifier structure used for classifying
%
% Input:
%  Ptrain - Sample matrix containing extracted features with
%           different filters on different column and different
%           landmarks on different rows
%  Ttrain - Target matrix containing the correct landmark number
%           for each row in Ptrain
%  trainList_          - File with full path containing training images
%                        and landmark files
%  testList_           - File with full path containing testing images
%                        and landmark files
%  cls_                - Name of an image category
%  conf2               - Structure containing information about parameters
%                        for bank of gabor filters
%
%  <optional>
% 'tempSaveDir'        - Output directory for all produced files
%                        (DEFAULT: '/tmp') - Note that you should
%                        specify this carefully as the system temp is
%                        cleaned periodically
% 'debugLevel'         - {0<DEFAULT>,1,2}
% 'trainSaveFile'      - File where Ptrain and Ttrain saved under
%                        'tempSaveDir' - used if Ptrain == Ttrain == []
%                        (DEFAULT:
%                        'evex_extracttrainfeatures_trainfeatures.mat')
% 'classifierSaveFile' - File where the classifier struct saved
%                        (DEFAULT:
%                        'evex_extracttrainfeatures_trainfeatures.mat')
% 'normInput'          - Whether the input vectors are normalised to
%                        1: unit energy (DEFAULT) or
%                        0: not normalised at all
% 'numberTransform'    - Numerical transform of possible complex
%                        valued inputs:
%                         1: abs()
%                         2: [real() imag()]
%                         3: [abs() angle()]
%                         4: [abs() sin(angle()) cos(angle())];
%                         otherwise: None (i.e. complex numbers) (DEFAULT)
% 'gmmbEMType'         - GMM estimation method (ref. [2])
%                        'FJ' (DEFAULT)
% 'gmmbParam_FJ_Cmax'  - Max number for components for FJ method
%                        (DEFAULT=10)
%
% See also O3D2D_EVEX_EXTRACTTRAINFEATURES.M,
%          O3D2D_EVEX_EXTRACTIMAGEFEATURES.M, GMMB_CREATE.M and
%          GMMB_CLASSIFY .
%
% References:
%
%  [1] Kamarainen, J.-K., A revised simple Gabor feature space:
%  supervised local part detector, Image and Vision Computing,
%  2011.
%
%  [2] Kamarainen, J.-K., Ilonen, J., Learning and Detection of
%  Object Landmarks in Canonical Object Space, 20th Int. Conf. on
%  Pattern Recognition (ICPR2010)  (Istanbul, Turkey, 2010).
%
%  [3] Riabchenko, E., Kamarainen, J.-K., Chen, Ke, Learning
%  Generative Models of Object Parts from A Few Positive Examples,
%  22nd Int. Conf. on Pattern Recognition (ICPR2014) (Stockholm, 2014)
%
% Author(s):
%    Joni Kamarainen <Joni.Kamarainen@tut.fi>
%    Jarmo Ilonen <Jarmo.Ilonen@lut.fi>
%    Ekaterina Riabchenko <Ekaterina.Riabchenko@lut.fi>
%
% Copyright:
%
%   Part detector (evidence extraction, *_evex_*) toolbox is
%   Copyright (C) 2008-2010 by Joni-Kristian Kamarainen and Jarmo
%   Ilonen.
%
function [classifierS] = o3d2d_evex_trainfeatureclassifier3(trainList_,testList_,cls_,...
    Ptrain_,Ttrain_,conf2,varargin);

% Parse input arguments
conf = struct(...
    'tempSaveDir', '/tmp',...
    'landmarkRoot','.',...
    'debugLevel', 0,...
    'Ncomp',24,...
    'classifierSaveFile', 'trainfeatureclassifier_classifier.mat',...
    'trainSaveFile', 'extracttrainfeatures_trainfeatures.mat',...
    'normInput', 1,...
    'numberTransform',0,...
    'gmmbEMType', 'FJ',...
    'gmmbParam_FJ_Cmax', 10,...
    'gmmbParam_EM_components', 5);
conf = mvpr_getargs(conf,varargin);

if (isempty(Ptrain_) & isempty(Ttrain_))
    % Read extracted features as not given
    fprintf(['   Loading training features from ''' conf.trainSaveFile ...
        '''...']);
    try
        load(fullfile(conf.tempSaveDir,conf.trainSaveFile),...
            'Ptrain','Ttrain');
    catch
        error('Ptrain and Ttrain empty and cannot read training data!');
    end;
    fprintf('done!\n');
end;


% Normalize responses if requested
if (conf.normInput)
    fprintf('   Normalizing responses...');
    nPtrain = mvpr_sg_normalizesamplematrix(Ptrain);
    fprintf('Done!\n');
else
    nPtrain=Ptrain;
    fprintf('   features not normalised, OK?\n');
end;


% number transform
if (conf.numberTransform == 1)
    nPtrain=abs(nPtrain);
elseif (conf.numberTransform==2)
    nPtrain=[real(nPtrain) imag(nPtrain)];
elseif (conf.numberTransform==3)
    nPtrain=[abs(nPtrain) angle(nPtrain)];
elseif (conf.numberTransform==4)
    nPtrain=[abs(nPtrain) sin(angle(nPtrain)) cos(angle(nPtrain))];
else
    % complex is the default (nothing needs to be done)
end;

newPtrain = []; newTtrain = [];

tmpP = nPtrain;
tmpT = Ttrain;
newTtrain = Ttrain;
numOfFeats = max(Ttrain);
numOfExamples = length(Ttrain)/numOfFeats;
eigVect = []; ind = 0; indG = 0;
category = cls_;

%%
chosenFeats = [];
flag = 1;
max_det = 0;
max_ind = 0;
n = 9;
%%
trainSaveFile = fullfile(conf2.caltech101_dir,conf2.file_list_train);
trainNum = numOfExamples;
% use not more than 200 ims for validation
if trainNum>200
    tmp1 = randperm(trainNum);
    valInds = tmp1(1:200);
    clear tmp1
else
    valInds = 1:trainNum;
end
valInds = sort(valInds,'ascend');
numVal = length(valInds);

% choose corrext N for N-fold
for i = 1:10
    N = i;
    if ((N-1)*numVal/N > 25) && numVal/N <= 20
        break
    end
end

%%
flag = 1;
train = 1;
if train
    inds = zeros(100,n);
    cumHist = cell(size(inds,1),1);
    evalP = cell(size(inds,1),1);
    while flag
        for i = 1:size(inds,1)
            fprintf(sprintf('\n %s, iteration %d \n',cls_,i));
            
            tmp2 = randperm(size(nPtrain,2));% choose n features
            inds(i,:) = tmp2(1:n);
            
  %% N-fold loop          
            for cnt = 1:N
                val_train = sort(valInds(1:ceil((N-1)*numVal/N)),'ascend');
                val_val = sort(valInds(ceil((N-1)*numVal/N)+1:numVal),'ascend');
                
                %%
                valSaveFile = strrep(trainSaveFile,'train','val');
                fr = mvpr_lopen(trainSaveFile,'read');
                fw = mvpr_lopen(valSaveFile,'write');
                % write a validation list of files for training classifier
                c = 1;
                for j = 1:trainNum
                    listEntries = mvpr_lread(fr);
                    if c<=length(val_train) && j==val_train(c)
                        mvpr_lwrite(fw,sprintf('%s %s',listEntries{1},listEntries{2}));
                        c = c+1;
                    end
                end
                mvpr_lclose(fw)
                mvpr_lclose(fr)
                
                valSaveFile = strrep(trainSaveFile,'train','val_val');
                fr = mvpr_lopen(trainSaveFile,'read');
                fw = mvpr_lopen(valSaveFile,'write');
                % write a validation list of files for testing classifier
                c = 1;
                for j = 1:trainNum
                    listEntries = mvpr_lread(fr);
                    if c<=length(val_val) && j==val_val(c)
                        mvpr_lwrite(fw,sprintf('%s %s',listEntries{1},listEntries{2}));
                        c = c+1;
                    end
                end
                mvpr_lclose(fw)
                mvpr_lclose(fr)
                
                valInds = [valInds(ceil((N-1)*numVal/N)+1:numVal), valInds(1:ceil((N-1)*numVal/N))];
                %%              
                newPtrain = tmpP(:,inds(i,:));
                newTtrain = tmpT;
                
                pointInds = [];
                for k = 1:numOfFeats
                    pointInds = [pointInds, (val_train-1)*numOfFeats+k];
                end
                newPtrain = newPtrain(pointInds,:);
                newTtrain = newTtrain(pointInds);
                
                newPtrain = newPtrain./repmat(sqrt(sum(abs(newPtrain).^2,2)),1,size(newPtrain,2));
                classifierS{1} = gmmb_create(newPtrain,newTtrain,...
                    'FJ','Cmax',conf.gmmbParam_FJ_Cmax);
                tmpClsr{i} = classifierS{1};
                Ttrainp(:,i) = gmmb_classify(classifierS{1},newPtrain);
                flag = 0;
                save(fullfile(conf.tempSaveDir,conf.classifierSaveFile),...
                    'classifierS');
                indG = inds(i,:);
                save(fullfile(conf.tempSaveDir,sprintf('indG_%s.mat',cls_)),...
                    'indG');
                % verification/evaluation of randomized GMM
                o3d2d_evex_extractimagefeatures_files2(...
                    [],...
                    strrep(fullfile(conf2.caltech101_dir,conf2.file_list_train),'train','val_val'),...
                    category,...
                    'iteration', i,...
                    'imageRoot',conf2.caltech101_dir,...
                    'landmarkRoot',conf2.caltech101_dir,... % for debug plot
                    'tempSaveDir', conf2.temp_save_dir,...
                    'debugLevel', conf2.debugLevel,...
                    'gabor_fmax', conf2.gabor.fmax,...
                    'gabor_fnum', conf2.gabor.fnum,...
                    'gabor_thetanum', conf2.gabor.thetanum,...
                    'gabor_k', conf2.gabor.k,...
                    'gabor_scaleShifts', conf2.gabor_scaleShifts,...
                    'gabor_orientShifts', conf2.gabor_orientShifts);
                
                evalP{i} = o3d2d_evex_evaluatetestresults_files(...
                    conf2.genLm,...
                    category,...
                    strrep(fullfile(conf2.caltech101_dir,conf2.file_list_train),'train','val_val'),...
                    'imageRoot',conf2.caltech101_dir,...
                    'landmarkRoot',conf2.caltech101_dir,...
                    'tempSaveDir', conf2.temp_save_dir,...
                    'debugLevel', 0,...
                    'plotSaveFile',fullfile(conf2.temp_save_dir),...
                    'noPlot', true);
                
                cumHist{i} = [cumHist{i}; evalP{i}.totFoundCumHist(2,5)];% measure of accuracy (10% of bb and 5 candidates fetched)
                
                if cnt == 1 meanCH = cumHist{i}; end
                
                if cnt>1 && abs(mean(cumHist{i})-meanCH)/meanCH<0.05
                    break
                end
                meanCH = mean(cumHist{i});
            end
            cumHist{i} = mean(cumHist{i});
        end
        
        totFoundCumHist = zeros(length(evalP),1);
        for i = 1:length(evalP)
            totFoundCumHist(i) = cumHist{i};
        end
        flag = 0;
    end
    
% choosing 5 best performing random combinations of filters
    classifierS = cell(5,1);
    indG = zeros(5,size(inds,2));
    clear classifierS
    for cand = 1:5
        [~, cls_ind] = max(totFoundCumHist);
        totFoundCumHist(cls_ind) = 0;
        indG(cand,:) = inds(cls_ind,:);
        classifierS{cand} = tmpClsr{cls_ind};
    end
    
save(fullfile(conf.tempSaveDir,conf.classifierSaveFile),...
    'classifierS');
save(fullfile(conf.tempSaveDir,sprintf('indG_%s.mat',cls_)),...
    'indG');
end
%% extracting object parts for the training set
% load(fullfile(conf.tempSaveDir,conf.classifierSaveFile));
% load(fullfile(conf.tempSaveDir,sprintf('indG_%s.mat',cls_)));

[thldPlanes,thldLms] = o3d2d_evex_extractimagefeatures_files2(...
    [],...
    fullfile(conf2.caltech101_dir,conf2.file_list_train),...
    category,...
    'iteration', 000,...
    'imageRoot',conf2.caltech101_dir,...
    'landmarkRoot',conf2.caltech101_dir,... % for debug plot
    'tempSaveDir', conf2.temp_save_dir,...
    'debugLevel', conf2.debugLevel,...
    'gabor_fmax', conf2.gabor.fmax,...
    'gabor_fnum', conf2.gabor.fnum,...
    'gabor_thetanum', conf2.gabor.thetanum,...
    'gabor_k', conf2.gabor.k,...
    'gabor_scaleShifts', conf2.gabor_scaleShifts,...
    'gabor_orientShifts', conf2.gabor_orientShifts);

det = o3d2d_evex_evaluatetestresults_files(...
    conf2.genLm,...
    category,...
    fullfile(conf2.caltech101_dir,conf2.file_list_train),...
    'imageRoot',conf2.caltech101_dir,...
    'landmarkRoot',conf2.caltech101_dir,...
    'tempSaveDir', conf2.temp_save_dir,...
    'debugLevel', 0,...
    'plotSaveFile',fullfile(conf2.temp_save_dir,[sprintf('%s_%d_train',category,n) '.png']),...
    'noPlot', false);

fprintf('Done!\n');
