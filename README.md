# GABOR OBJECT DETECTOR CODE

For all details of installation, running etc. see the project Wiki page: https://bitbucket.org/EkaterinaRiabchenko/gabor_object_detector_code/wiki/Home

This repository contains code to replicate the experiments published in several scientific papers using Gabor features in object detection.

